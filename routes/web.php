<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * @project: LaraBid
 * @website: https://themeqx.com
 */

Auth::routes();

//Dashboard Route
Route::group(['prefix'=>'admin', 'middleware' => 'admin'], function(){
      // Authencation and home page in admin
      Route::get('/','Admin\HomeController@index');
      Route::get('/logout','Admin\HomeController@logout');

      // Action in admin User
      Route::get('/users','Admin\UserController@index');
      Route::post('/users','Admin\UserController@index');
      Route::get('/delete-user/{id}','Admin\UserController@delete');
      Route::get('/add-user','Admin\UserController@add');
      Route::post('/add-user','Admin\UserController@add');
      Route::get('/edit-user/{id}','Admin\UserController@edit');
      Route::post('/edit-user/{id}','Admin\UserController@edit');


      Route::post('/import-excel','Admin\UserController@import');

      /**
       * User : Duong Hoai Sang
       * Note : Route group allocatecapitals
       */
      Route::group(['prefix'=>'allocatecapitals'], function(){
            Route::get('/','Admin\AllocatecapitalsController@showListAllocatecapitals');
            Route::get('/get-list-allocatecapitals','Admin\AllocatecapitalsController@getListAllocatecapitals')->name('get_list_allocatecapitals');
            Route::post('/','Admin\AllocatecapitalsController@showListAllocatecapitals');

            Route::get('/addallocatecapitals','Admin\AllocatecapitalsController@addAllocatecapitals')->name('add_allocatecapitals');
            Route::post('/addallocatecapitals','Admin\AllocatecapitalsController@addAllocatecapitals')->name('add_allocatecapitals');
            //Edit depositaddress.
            Route::get('/editallocatecapitals/{id}','Admin\AllocatecapitalsController@editAllocatecapitals')->name('edit_allocatecapitals');
            Route::post('/editallocatecapitals/{id}','Admin\AllocatecapitalsController@editAllocatecapitals')->name('edit_allocatecapitals');
            //Delete depositaddress.
            Route::get('/deleteallocatecapitals/{id}','Admin\AllocatecapitalsController@deleteAllocatecapitals')->name('detele_allocatecapitals');
      });

      /**
       * User : Duong Hoai Sang
       * Note : Route group dailybalances
       */
      Route::group(['prefix'=>'dailybalances'], function(){
            Route::get('/','Admin\DailyBalancesController@list');
            Route::post('/','Admin\DailyBalancesController@list');
            Route::get('/get-list-daily-balances','Admin\DailyBalancesController@getListDailyBalances')->name('get_list_daily_balances');

            Route::get('/total-daily','Admin\DailyBalancesController@total');

            Route::get('/exchangesummaries','Admin\DailyBalancesController@listExchangesummaries');
            Route::get('/get-daily-balances-exchangesummaries','Admin\DailyBalancesController@getListDailyBalancesExchangesummaries')->name('get_daily_balances_exchangesummaries');

            Route::get('/summaries','Admin\DailyBalancesController@listSummaries');
            Route::get('/get-daily-balances-summaries','Admin\DailyBalancesController@getListSummaries')->name('get_daily_balances_summaries');

            Route::get('/dailyprofit','Admin\DailyBalancesController@listDailyprofit');
            Route::get('/get-daily-balances-dailyprofit','Admin\DailyBalancesController@getListDailyprofit')->name('get_daily_balances_dailyprofit');

            Route::get('/dailyprofit-params','Admin\DailyBalancesController@getListDailyprofitWithParam');

      });

      /**
       * User : Duong Hoai Sang
       * Note : Route group depositaddress
       */
      Route::group(['prefix'=>'depositaddress'], function(){
            //Get list depositaddress.
            Route::get('/','Admin\DepositaddressController@showListDepositaddress');
            Route::get('/get-list-depositaddress','Admin\DepositaddressController@getListDepositaddress')->name('get_list_depositaddress');
            Route::post('/','Admin\DepositaddressController@showListDepositaddress');
            //Add depositaddress.
            Route::get('/adddepositaddress','Admin\DepositaddressController@addDepositaddress')->name('add_depositaddress');
            Route::post('/adddepositaddress','Admin\DepositaddressController@addDepositaddress')->name('add_depositaddress');
            //Edit depositaddress.
            Route::get('/editdepositaddress/{id}','Admin\DepositaddressController@editDepositaddress')->name('edit_depositaddress');
            Route::post('/editdepositaddress/{id}','Admin\DepositaddressController@editDepositaddress')->name('edit_depositaddress');
            //Delete depositaddress.
            Route::get('/deletedepositaddress/{id}','Admin\DepositaddressController@deleteDepositaddress')->name('detele_depositaddress');
      });

      /**
       * User : Duong Hoai Sang
       * Note : Route group arbitragesignals
       */
      Route::group(['prefix'=>'arbitragesignals'], function(){
            Route::get('/','Admin\ArbitragesignalsController@showListArbitragesignals');
            Route::get('/get-list-arbitragesignals','Admin\ArbitragesignalsController@getListArbitragesignals')->name('get_list_arbitragesignals');
      });

      /**
       * User : Duong Hoai Sang
       * Note : Route group exchanges
       */
      Route::group(['prefix'=>'exchanges'], function(){
            Route::get('/','Admin\ExchangesController@showListExchanges');
            Route::get('/get-list-exchanges','Admin\ExchangesController@getListExchanges')->name('get_list_exchanges');
      });

      /**
       * User : Duong Hoai Sang
       * Note : Route group markets
       */
      Route::group(['prefix'=>'markets'], function(){
            Route::get('/','Admin\MarketsController@showListMarkets');
            Route::post('/','Admin\MarketsController@showListMarkets');
            Route::get('/get-list-markets-all','Admin\MarketsController@getListMarketsAll')->name('get_list_markets_all');
            Route::get('/get-list-markets','Admin\MarketsController@getListMarkets')->name('get_list_markets');
      });

      /**
       * User : Duong Hoai Sang
       * Note : Route group userarbitrages
       */
      Route::group(['prefix'=>'userarbitrages'], function(){
            Route::get('/','Admin\UserarbitragesController@showListUserarbitrages');
            Route::post('/','Admin\UserarbitragesController@showListUserarbitrages');
            Route::get('/get-list-userarbitrages','Admin\UserarbitragesController@getListUserarbitrages')->name('get_list_userarbitrages');
            Route::get('/start-trade/{id}','Admin\UserarbitragesController@start');
            Route::get('/stop-trade/{id}','Admin\UserarbitragesController@stop');
            Route::post('/update-execute-signal','Admin\UserarbitragesController@updateIsExecuteSignal');
            //Add depositaddress.
            Route::get('/add','Admin\UserarbitragesController@add');
            Route::post('/add','Admin\UserarbitragesController@add');
            //Edit depositaddress.
            Route::get('/edit/{id}','Admin\UserarbitragesController@edit');
            Route::post('/edit/{id}','Admin\UserarbitragesController@edit');
            //Delete depositaddress.
            Route::get('/delete/{id}','Admin\UserarbitragesController@delete');
      });

      /**
       * User : Duong Hoai Sang
       * Note : Route group arbitragetransactions
       * {page?}
       */
      Route::group(['prefix'=>'arbitragetransactions'], function(){
            Route::get('/','Admin\ArbitragetransactionsController@showListArbitragetransactions');
            Route::get('/get-list-arbitragetransactions','Admin\ArbitragetransactionsController@getListArbitragetransactions')->name('get_list_arbitragetransactions');
      });


});
Route::get('/','HomeController@index');
Route::group([ 'middleware' => 'admin'], function(){
      // Authencation and home page in admin


});

Route::group(['prefix'=>'admin'], function(){
      Route::get('/verify','Admin\UserController@verify');
      Route::post('/verify','Admin\UserController@verify');
      Route::get('/login','Admin\UserController@login');
      Route::post('/login','Admin\UserController@login')->name('login_admin');

});
