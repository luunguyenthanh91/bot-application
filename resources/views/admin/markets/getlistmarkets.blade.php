@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container-fluid">

    <div class="page-header">
      <h4 class="page-title">Markets</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Markets</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>
          <div class="row row-cards">
            <div class="col-lg-3">
              <div class="row">

                <div class="col-md-12 col-lg-12">
                  <div class="card">

                    <div class="card-header">
                      <div class="card-title"> Search</div>
                    </div>

                    <div class="card-body">

                      <div class="form-group col-xl-12 col-md-12 float-left">
                        <label class="form-label">EXNAME</label>
                        <select id="event_id" name="condition[event_id]" class="form-control custom-select col-xl-12 col-md-12">

                          </select>
                        <button type="button" class="btn btn-primary btn_search col-xl-12 col-md-12 col-lg-12">
                          <i class="fa fa-search " aria-hidden="true"></i>
                        </button>
                      </div>



                      <form method="post" action="" id="form_search">
                          @csrf
                          <input type="hidden" name="condition[event_id]" id="event_id_val">
                      </form>


                    </div>

                  </div>

                </div>
              </div>
            </div>


							<div class="col-lg-9">
								<div class="card mt-5 store">
                  @if($message != '')
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$message}}
                  </div>
                  @endif
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th class="text-center">MarketName</th>
                            <th class="text-center">ExactMarketName</th>
                            <th class="text-center">ExchangeName</th>
                            <th class="text-center">Currency</th>
                            <th class="text-center">BaseCurrency</th>
                          </tr>
                        </thead>
                        <tbody id="myList" class="row-data-load-daily-balances">
                        <!-- Load data tai day -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->

  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <script type="text/javascript">
        $( document ).ready(function() {
          // setInterval(function() {
          // }, 5000);
          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_list_exchanges")}}',
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='<option value="">--Tất Cả--</option>';

                arraydata.forEach(function(object) {
                  //Check URL website.
                  if(object.WebUrl=='' || object.WebUrl == undefined){
                    webUrl = '...';
                  }else{
                    webUrl = object.WebUrl;
                  }
                  if(('{{@$condition['event_id']}}') == object.Id){
                      valueColumn+= '<option selected value="'+object.Id+'">'+object.Name+'</option>';
                  }else{
                    valueColumn+= '<option value="'+object.Id+'">'+object.Name+'</option>';
                  }

                });
                $("#event_id").html(valueColumn);
              },error:function(){
                  console.log(data);
              }
          });

          $('.btn_search').on("click",function(){
              var event_id = $("#event_id").val();
              $("#event_id_val").val(event_id);
              $("#form_search").submit();
          });

          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_list_markets_all")}}',
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='';
                arraydata.forEach(function(object) {
                  //console.log(object.ExchangeName);
                  valueColumn+= "<tr><td class='align-middle text-center'>"+object.MarketName+"</td>"+
                  "<td class='align-middle text-center'>"+object.ExactMarketName+"</td>"+
                  "<td class='align-middle text-center'>"+object.ExchangeName+"</td>"+
                  "<td class='align-middle text-center'>"+object.Currency+"</td>"+
                  "<td class='align-middle text-center'>"+object.BaseCurrency+"</td>"+
                  "</tr>"
                });
                $(".row-data-load-daily-balances").html(valueColumn);
              },error:function(){
                  console.log(data);
              }
          });
        });
    </script>
@endsection
