@extends('layouts.admin_main')

@section('content')
<style type="text/css">
  hr.tab{margin:2px;}
</style>
<ng-controller ng-controller="AppController as demo">
    <div class="content-area">
      <div class="container-fluid">

        <div class="page-header">
          <h4 class="page-title">Arbitragesignals</h4>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/admin">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Arbitragesignals</li>
          </ol>
        </div>
        <div class="row row-cards">
          <div class="col-lg-12">
            <div>
              <div class="row row-cards">
    							<div class="col-lg-12">
    								<div class="card mt-5 store">
                      @if($message != '')
                      <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{$message}}
                      </div>
                      @endif
                      <div class="e-table">
                        <div class="table-responsive table-lg">
                          <table class="table table-bordered">
                            <thead>
                              <tr>

                                <th class="text-center">Id</th>
                                <th class="text-center">BUY/PRICE/QTY</th>
                                <th class="text-center">SELL/PRICE/QTY</th>
                                <th class="text-center">PAIR</th>
                                <th class="text-center">BaseCurrency </th>
                                <th class="text-center">Currency</th>
                                <th class="text-center">% PROFIT</th>
                                <th class="text-center">TIME (ms)</th>
                              </tr>
                            </thead>
                            <tbody id="myList" class="row-data-load-daily-balances">
                              <tr ng-repeat="item in allsignal" ng-class="item.status == 1 ? 'red-tabel-row' : ''">
                                <td class='align-middle text-center'>((item.Id))</td>
                                <td class='align-middle text-center'>((item.BuyExchangeName)) / ((item.BuyPrice)) / ((item.BuyAmount))</td>
                                <td class='align-middle text-center'>((item.SellExchangeName)) / ((item.SellPrice)) / ((item.SellAmount))</td>
                                <td class='align-middle text-center'>((item.MarketName))</td>
                                <td class='align-middle text-center'>((item.BaseCurrency))</td>
                                <td class='align-middle text-center'>((item.Currency))</td>
                                <td class='align-middle text-center'>((item.ProfitPct))</td>
                                <td class='align-middle text-center'>((item.CheckingDuration))</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
    							</div>
    						</div>
            </div>
          </div>
        </div>
      </div>
      <!--footer-->

      @include('admin.component.footer')

      <!-- End Footer-->
    </div>
</ng-controller>
@endsection





@section('page-js')


<script>
  customInterpolationApp.controller('AppController', function($scope, $http) {
      $scope.allsignal = [];
      function loadRealtimeArbitragesignals(){
          $http.get("{{route("get_list_arbitragesignals")}}")
          .then(function(response) {
              var obj = JSON.parse(response.data.data);
              var arraydata = obj.Result;
              angular.forEach($scope.allsignal, function(value, key) {
                  value.status = 0;
              });
              arraydata.reverse();

              angular.forEach(arraydata, function(value, key) {
                  if (!$scope.allsignal.some((item) => item.Id == value.Id)) {
                      value.status = 1;
                      $scope.allsignal.unshift(value);
                  }

              });
          });
      }
      // start get data


      loadRealtimeArbitragesignals();
      setInterval(function() {
        loadRealtimeArbitragesignals();
      }, 5000);


  });
</script>



@endsection
