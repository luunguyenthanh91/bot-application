@extends('layouts.admin_main')

@section('content')
<style type="text/css">
  hr.tab{margin:2px;}
</style>
<div class="content-area">
  <div class="container-fluid">

    <div class="page-header">
      <h4 class="page-title">Depositaddress</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Depositaddress</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>
          <div class="row row-cards">

              <div class="col-lg-3">
                <div class="row">

                  <div class="col-md-12 col-lg-12">
                    <div class="card">

                      <div class="card-header">
                        <div class="card-title"> Lọc Dữ Liệu</div>
                      </div>

                      <div class="card-body">

                        <div class="form-group col-xl-12 col-md-12 float-left">
                          <label class="form-label">Example</label>
                          <select id="event_id" name="condition[event_id]" class="form-control custom-select col-xl-12 col-md-12">
                            <option value="">--Tất Cả--</option>
                                  <option value="5">Toyota Event</option>
                                  <option value="4">Quản Trị Viên</option>
                            </select>
                          <button type="button" class="btn btn-primary btn_search col-xl-12 col-md-12 col-lg-12">
                            <i class="fa fa-search " aria-hidden="true"></i>
                          </button>
                        </div>

                        <div class="form-group col-xl-12 col-md-12 float-left">
                            <label class="form-label">Thêm Dữ Liệu</label>
                            <a href="/admin/depositaddress/adddepositaddress" class="btn btn-primary col-xl-12 col-md-12 col-lg-12">
                              <i class="fa fa-calendar-plus-o " aria-hidden="true"></i>
                            </a>
                        </div>


                        <form method="post" action="" id="form_search">
                            @csrf
                            <input type="hidden" name="condition[event_id]" id="event_id_val">
                        </form>


                      </div>

                    </div>

                  </div>
                </div>
              </div>


							<div class="col-lg-9">
								<div class="card store">
                  @if(session('message'))
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <strong>{{ session('message') }}</strong>
                      </div>
                  @endif
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table id="depositaddress" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                          <tr>
                            <th class="text-center">ExName</th>
                            <th class="text-center">Currency</th>
                            <th class="text-center">Action</th>
                            <th class="text-center">Address / Address Tag</th>
                          </tr>
                        </thead>
                        <tbody class="row-data-load-daily-balances">
                        <!-- Load data tai day -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->

  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')

    <link href="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet" />
    <script src="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('admins/assets/js/sweet-alert.js') }}"></script>
    <script>
      $( document ).ready(function() {
        $("body").on("click",".delete_conform",function(){
            var id = $(this).attr('id');
            swal({
              title: "Xác nhận",
              text: "Bạn đồng ý xoá?",
              type: "warning",
              showCancelButton: true,
              confirmButtonText: 'Có',
              cancelButtonText: 'Không'
            }, function(inputValue) {
                if(inputValue){
                  var url= "/admin/depositaddress/deletedepositaddress/"+id;
                  window.location = url;
                }

            });
        });
      });
    </script>

    <script type="text/javascript">
        $( document ).ready(function() {
          // setInterval(function() {
          // }, 5000);

          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_list_depositaddress")}}',
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='';

                arraydata.forEach(function(object) {
                  //Check URL website.
                  if(object.WebUrl=='' || object.WebUrl == undefined){
                    webUrl = '...';
                  }else{
                    webUrl = object.WebUrl;
                  }
                  valueColumn+= "<tr><td class='align-middle text-center'>"+object.ExchangeName+"</td><td class='align-middle text-center'>"+object.Currency+"</td>"+
                  '<td class="align-middle text-center"><a class="btn btn-info btn-sm" href="depositaddress/editdepositaddress/'+object.Id+'"><i class="mdi mdi-account-edit"></i></a>' + '<a class="btn btn-danger btn-sm delete_conform" id='+object.Id+'><i class="fa fa-trash"></i></a></td>'+
                  "<td class='align-middle text-center'>"+object.Address+"<br/>" +object.AddressTag+"</td>"+
                  "</tr>"
                });
                $(".row-data-load-daily-balances").html(valueColumn);
              },error:function(){
                  console.log(data);
              }


          });
        });
    </script>

@endsection
