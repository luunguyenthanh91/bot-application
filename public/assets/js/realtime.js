var app = require('express')();
var http = require('http').Server(app);

const https = require('https');
const fs = require('fs');

var server = https.createServer({
                key: fs.readFileSync('./key.pem'),
                cert: fs.readFileSync('./cert.pem')
             },app);
server.listen(4322);

var io = require('socket.io').listen(server);
// var io = require('socket.io')(https);
//
// const options = {
//   key: fs.readFileSync('./key.pem'),
//   cert: fs.readFileSync('./cert.pem')
// };


io.on('connection', function(socket){
  console.log('a user connected'+socket.id);
  socket.on('realtime_bid', function(data_update){
    // console.log(data_update);
    io.sockets.emit("sever_update_bid",data_update);
  });
});

// http.listen(4321, function(){
//   console.log('listening on *:4321');
// });

// https.createServer(options, function (req, res) {
//   res.writeHead(200);
//   res.end("hello world\n");
// }).listen(4322);
