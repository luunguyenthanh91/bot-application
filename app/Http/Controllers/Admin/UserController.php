<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use App\Country;
use App\Favorite;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use App\Helpers\SpeedSMSAPI;
use Session;
use Excel;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $redirectTo = '/dashboard';

    public function import(Request $request)
    {
      if($request->hasFile('file_excel')){
          $path = $request->file('file_excel');
          $destinationPath = 'uploads/file/';
          $path->move($destinationPath,$path->getClientOriginalName());
          $name = $destinationPath.$path->getClientOriginalName();

          $data = \Excel::toArray('', $name, null, \Maatwebsite\Excel\Excel::TSV)[0];

          foreach ($data as $key => $value) {
              $user = User::where('email',$value[2])->where('type_login',1)->first();
              if(!$user){
                  $user = new User();
                  $user->name = $value[0];
                  $user->phone = $value[1];
                  $user->email = $value[2];
                  $user->address = $value[3];
                  $user->password = bcrypt($value[4]);
                  $user->save();
              }
          }

       }
      Session::put('message', "Import data success.");
      return redirect('/admin/users');

    }

    public function edit(Request $request,$id)
    {
        $message = '';
        if ($request->isMethod('post')) {
            $data = $request->data;
            $validator = Validator::make($data, [
                    'name' => 'required',
                    'phone' => 'required|numeric',
                    'address' => 'required',
                ],[
                  'name.required' => 'Name not empty',
                  'phone.required' => 'Phone not empty',
                  'phone.numeric' => 'Phone is numeric',
                  'address.required' => 'Address not empty',
                ]
            );

            if ($validator->fails()) {
                $errors = $validator->errors();
                return view('admin.pages.edit_user', compact('data','errors'));
            }

            if($data['password'] != $data['re_password'] && $data['password'] !='' ){
              $message = "Password New and Repeat Password Not Duplicate";
              return view('admin.pages.edit_user', compact('data','message'));
            }
            $user =  User::find($id);
            if($data['password'] != ''){
                $user->password = bcrypt($data['password']);
            }
            $user->name = $data['name'];
            $user->address = $data['address'];
            $user->phone = $data['phone'];
            $user->save();

            Session::put('message', "Update profile success.");
            return redirect('/admin/users');
        }else{
          $data = User::find($id);
        }


        return view('admin.pages.edit_user', compact('data','message'));
    }


    public function add(Request $request)
    {
        $message = '';
        if ($request->isMethod('post')) {
            $data = $request->data;
            $validator = Validator::make($data, [
                    'name' => 'required',
                    'email' => 'required',
                    'phone' => 'required|numeric',
                    'address' => 'required',
                    'password' => 'required',
                    're_password' => 'required'
                ],[
                  'name.required' => 'Name not empty',
                  'email.required' => 'Email not empty',
                  'phone.required' => 'Phone not empty',
                  'phone.numeric' => 'Phone is numeric',
                  'address.required' => 'Address not empty',
                  'password.required' => 'Password not empty',
                  're_password.required' => 'Repeat password not empty'
                ]
            );

            if ($validator->fails()) {
                $errors = $validator->errors();
                return view('admin.pages.add_user', compact('data','errors'));
            }
            $user = User::where('email',$data['email'])->where('type_login',1)->first();
            if($user){
                $message = "Email exists. Please enter email other";
                return view('admin.pages.add_user', compact('data','message'));
            }
            if($data['password'] != $data['re_password']){
              $message = "Password New and Repeat Password Not Duplicate";
              return view('admin.pages.add_user', compact('data','message'));
            }

            $datainsert = [
                'name'        => $data['name'],
                'email'       => $data['email'],
                'address'     => $data['address'],
                'phone'       => $data['phone'],
                'password'    => bcrypt($data['password'])

            ];
            $user = User::create($datainsert);

            Session::put('message', "Create account success.");
            return redirect('/admin/users');
        }else{
          $data = [];
        }


        return view('admin.pages.add_user', compact('data','message'));
    }


    public function delete(Request $request,$id){
        $user = User::where('id',$id)->first();
        if(@$user->role != 'admin'){
              $user->delete();
              Session::put('message', "Delete account success.");
        }else{
            Session::put('message', "Account Admin can not delete.");
        }


        return redirect('/admin/users');

    }

    public function index(Request $request){
        $menu = 'users';
        if(!$request->page){
          $page = 1;
        }else{
            $page = $request->page;
        }
        $users = User::orderby('id','DESC');
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('user_condition', $condition);
        }
        else{
            if(Session::get('user_condition')){
                $condition = Session::get('user_condition');
            }
        }
        if(@$condition['phone'] != ''){
            $users = $users->where('phone','like',"%".$condition['phone']."%");
        }

        if(@$condition['type_login'] != ''){
            $users = $users->where('type_login','=',$condition['type_login']);
        }

        if(@$condition['email'] != ''){
            $users = $users->where('email','like',"%".$condition['email']."%");
        }

        $users = $users->paginate(20);
        // echo "<pre>";
        // print_r($users->link());
        // echo "</pre>";
        // die;
        if(Session::get('message')){
            $message = Session::get('message');
            Session::put('message', "");
        }else{
            $message = "";
        }

        return view('admin.pages.managerUsers',compact('menu','users','page' , 'condition' , 'message'));
    }


    public function verify(Request $request)
    {
        $user_login = json_decode(Session::get('login_user'));
        if($user_login == ''){
            return redirect('/admin/login/');
        }
        if ($request->isMethod('post')) {
            $code_verify = Session::get('code_verify');
            if($request->code == $code_verify){
                // print_r($code_verify);die;

                $user = User::where('email',$user_login->email)->first();
                $user->save();
                Auth::login($user);
                $user = Auth::user();
                $token = $user->createToken('LOGIN')->accessToken;
                $user['token'] = $token;
                return redirect('/admin/');
            }else if($request->code){
                $message = "Wrong confirmation code";
                // print_r($errors);die;
                return view('admin.auth.verify', compact('message'));
            }else{
                return view('admin.auth.verify');
            }
        }
        return view('admin.auth.verify');

    }
    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = \Validator::make($request->all(), [

                'email'    => 'required',
                'password' => 'required'
            ], [
                'email.required'    => 'Email not empty',
                'password.required' => 'Password not empty'
            ]);
            if($validator->fails()) {
                $errors = $validator->errors();
                // print_r($errors);die;
                return view('admin.auth.login', compact('errors'));
            }

            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                  $user_login = json_encode(['email' => $request->email, 'password' => $request->password]);
                  // require_once  "../../../../Helpers/SpeedSMSAPI.php";
                  $user = User::where('email',$request->email)->first();
                  $code_random = rand(100000,999999);
                 $smsAPI = new SpeedSMSAPI("vjbR07aBoXH57QxKn2oLGzQuegylxphU");

                 $smsAPI->getUserInfo();

                 $phones = ["$user->mobile"];


                 $content = "Code login to primearbitron : ".$code_random;
                 $type = 1;

                 $sender = 7;

                 $smsAPI->sendSMS($phones, $content, $type, $sender);
                  Session::put('login_user', $user_login);
                  Session::put('code_verify', $code_random);
                  Auth::logout();
                  return redirect('/admin/verify');

            }
            else
            {
                $message = "Username or password dose not exists";
                return view('admin.auth.login', compact('message'));
            }

        }
        $title = "";
        $users = "";

        return view('admin.auth.login', compact('title', 'users'));
    }





}
