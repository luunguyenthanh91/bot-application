@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container-fluid">

    <div class="page-header">
      <h4 class="page-title">Daily Balances</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Daily Balances</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>
          <div class="row row-cards">

							<div class="col-lg-12">
								<div class="card mt-5 store">

                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <div>
                          <div class="row row-cards">
                              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                  <div class="card crypt-marketing-values">
                                      <div class="card-header">
                                          <div class="card-title">Report Number Of Coins Remaining</div>
                                      </div>
                                      <div class="card-body">
                                          <div class="table-responsive">
                                              <table class="table border table-bordered mb-0">
                                                  <thead>
                                                      <tr>
                                                          <th>No</th>
                                                          <th>Exchanges</th>
                                                          <th>Currency</th>
                                                          <th>Available</th>
                                                          <th>Quantity</th>
                                                          <th>Balance</th>
                                                          <th>TotalUsdt</th>
                                                          <th>TotalBtc</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody class="text-gray">
                                                    @php $i = 1 @endphp
                                                    @foreach($data_report as $keys => $value)

                                                        @foreach($value as $key1 => $value1)
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td>&nbsp;{{$keys}}</td>
                                                                <td>{{$key1}}</td>
                                                                <td>$ {{$value1['Available']}}</td>
                                                                <td>$ {{$value1['InitialBalance']}}</td>
                                                                <td>@if($value1['Diff'] >= 0 ) <p style="color:blue">{{$value1['Diff']}}%</p> @else <p style="color:red">{{$value1['Diff']}}%</p> @endif  </td>
                                                                <td>$ {{$value1['TotalUsdt']}}</td>
                                                                <td>$ {{$value1['TotalBtc']}}</td>

                                                            </tr>
                                                            @php $i++ @endphp
                                                        @endforeach
                                                    @endforeach


                                                  </tbody>
                                              </table>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->

  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')

@endsection
