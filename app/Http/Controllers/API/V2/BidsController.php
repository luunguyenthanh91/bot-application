<?php

namespace App\Http\Controllers\API\V2;

use App\Ad;
use App\Bid;
use App\HB;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use App\User;

class BidsController extends Controller
{

    public function store(Request $request,$id)
    {

        try{
            $ads = Ad::find($id);
            $curent_bid = Bid::where('bids.ad_id','=',$id)->orderby('bids.id','DESC')->first();
            if($ads->flag == 1){
                return $this->dataError("Đấu giá đã kết thúc.",null,200);
            }
            // print_r($request->bid_amount);die;
            if($ads->user_id == Auth::user()->id){
                return $this->dataError("Bạn không được đấu giá chính đấu giá của bạn.",null,200);
            }
            if($curent_bid){
              if(@$curent_bid->is_accepted == 1){
                  return $this->dataError("Đấu giá đã kết thúc.",null,200);
              }
              if(@$curent_bid->bid_amount + $ads->next_min > $request->bid_amount){
                  return $this->dataErrorBids("Đấu giá tiếp theo phải lớn hơn hoặc bằng : ".number_format(($curent_bid->bid_amount + $ads->next_min), 0, '', ',') ." VNĐ",$curent_bid->bid_amount + $ads->next_min,[],200);
              }
            }else{
                if(@$ads->price + $ads->next_min > $request->bid_amount){
                    return $this->dataErrorBids("Đấu giá tiếp theo phải lớn hơn hoặc bằng : ".number_format(($ads->price + $ads->next_min), 0, '', ',') ." VNĐ",$ads->price + $ads->next_min,[],200);
                }
            }
            if($request->bid_amount >= $ads->buy_now ){
                  $bid = new Bid();
                  $bid->user_id = Auth::user()->id;
                  $bid->ad_id = $id;
                  $bid->is_accepted = 1;
                  $bid->bid_amount = $request->bid_amount;
                  $bid->save();
                  $ads->expired_at = date("Y-m-d H:i:s", strtotime("-1 days"));
                  $ads->flag = 1;
                  $ads->save();
                  $HB = HB::where('user_id','=',Auth::user()->id)->where('auction_id','=',$id)->first();
                  if(!$HB){
                        $HB = new HB();
                        $HB->user_id = Auth::user()->id;
                        $HB->auction_id = $id;
                        $HB->save();
                  }
                  return $this->dataSuccessBid('Đấu giá thành công',$bid,200,true);
            }else{

                $bid = new Bid();
                $bid->user_id = Auth::user()->id;
                $bid->ad_id = $id;
                $bid->is_accepted = 0;
                $bid->bid_amount = $request->bid_amount;
                $bid->save();
                $HB = HB::where('user_id','=',Auth::user()->id)->where('auction_id','=',$id)->first();
                if(!$HB){
                      $HB = new HB();
                      $HB->user_id = Auth::user()->id;
                      $HB->auction_id = $id;
                      $HB->save();
                }
                return $this->dataSuccessBid('Đấu giá thành công',$bid,200,false);
            }



        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }

    }

    public function show($id)
    {
          try{
              $bids = Bid::orderby('bids.id','DESC');
              $bids = $bids->join('users', 'users.id', '=', 'bids.user_id');
              $bids = $bids->where('bids.ad_id','=',$id);
              return $this->dataSuccess('Lấy đấu giá chi tiết thành công',$bids->get(),200);

          }
          catch (\Exception $exception)
          {
              return $this->dataError($exception->getMessage(),null,200);
          }
    }


    public function testPush(Request $request)
    {
       try{
           \FirebaseService::fcm($request->device_token,'Test Push','Push thành công.',[],'test_push');
           return $this->dataSuccess('Push thành công',null,200);
       }
       catch (\Exception $exception)
       {
           return $this->dataError($exception->getMessage(),null,200);
       }
    }

    public function getBidByUser(Request $request)
    {
        try{
            $HBs = HB::where('user_id','=',Auth::user()->id);
            $HBs = $HBs->paginate(10);

            foreach ($HBs as $key => $value) {
                $bids_flag = Bid::where('ad_id','=',$value['auction_id'])->where('user_id','=',Auth::user()->id)->orderby('bids.id','DESC')->first();
                $HBs[$key]['bid'] = $bids_flag;
                $HBs[$key]['auction'] = Ad::select("id","title","description","category_id","price","expired_at","next_min","buy_now","flag")->where('id','=',$value['auction_id'])->first();
                if($bids_flag['is_accepted'] == 1){
                    $HBs[$key]['status'] = "Win";
                }else{
                    if($HBs[$key]['auction']['flag'] == 1){
                        $HBs[$key]['status'] = "Lose";
                    }else{
                        $HBs[$key]['status'] = "Wating";
                    }
                }
            }
            return $this->dataSuccess('Lấy danh sách đấu giá thành công',$HBs,200);


        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }


    }

    public function getBidByUserDetail(Request $request , $id)
    {
        try{
            $bids = Bid::orderby('id','DESC');
            $bids = $bids->where('user_id','=',Auth::user()->id);
            $bids = $bids->where('ad_id','=',$id);
            $bids = $bids->get();
            $ads = Ad::select("id",'user_id',"title","description","category_id","price","expired_at","next_min","buy_now","flag")->where('id','=',$id)->first();
            $return_data = [];
            $check_win = Bid::where('ad_id','=',$id)->where('user_id','=',Auth::user()->id)->orderby('id','DESC')->first();
            if(@$check_win && @$check_win['is_accepted'] == 1){
                $return_data['status'] = 'Win';
                $return_data['seller'] = User::select("name", "email" , "phone" , "address", "photo")->where('id','=',$ads['user_id'])->first();
            }else{
                if($ads['flag'] == 1){
                    $return_data['status'] = 'Lose';
                }else{
                    $return_data['status'] = "Wating";
                }
                $return_data['seller'] = null;
            }
            $return_data['data'] = $bids;
            return $this->dataSuccess('Lấy danh sách đấu giá thành công',$return_data,200);
        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }


    }

}
