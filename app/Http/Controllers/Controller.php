<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Psr7\Response;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function dataSuccess($mes, $data = [],$code = 200)
    {
        return response()->json([
            'result'  => true,
            'message' => $mes,
            'data'    => $data,
            'code'  =>200

        ], $code);
    }

    public function dataSuccessBid($mes, $data = [],$code = 200 , $complete = false)
    {
        return response()->json([
            'result'  => true,
            'message' => $mes,
            'data'    => $data,
            'code'  =>200,
            'complete'  =>$complete,

        ], $code);
    }

    public function dataError($mes, $data = [], $code = 200)
    {
        return response()->json([
            'result'  => false,
            'message' => $mes,
            'data'    => $data,
            'code'    => $code
        ], $code);
    }

    public function dataErrorBids($mes, $price,$data = [], $code = 200)
    {
        return response()->json([
            'result'  => false,
            'message' => $mes,
            'price_next' => $price,
            'data'    => $data,
            'code'    => 200
        ], 200);
    }

    public function convert_slug($str)
    {
          if(!$str) return false;

          $utf8 = array(

          'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

          'd'=>'đ|Đ',

          'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

          'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',

          'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

          'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

          'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',

          );

          foreach($utf8 as $ascii=>$uni) $str = preg_replace("/($uni)/i",$ascii,$str);

          return $str;
    }

    /**
     * User : Duong Hoai Sang
     * Note : Get EndPoint API. Return json data.
     */

     public function getOutSide($url){
         try{
             //$client = new \GuzzleHttp\Client();
             $client = new Client();
             $option = array('exceptions' => false);
             $response = $client->get($url);
             $result = $response->getBody()->getContents();
             return $result;
         }catch (RequestException $e){
             print_r($e->getMessage());die;
         }
     }

    public function getEndPointApi($url){
        try{
            //$client = new \GuzzleHttp\Client();
            $client = new Client();
            $url = 'http://199.247.16.199:1000/'.$url;
            $option = array('exceptions' => false);
            $header = array('Authorization'=>'Bearer ' . 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjAsIlVzZXJOYW1lIjoiYWRtaW4iLCJTY29wZXMiOlsiYWRtaW4iXSwiRXhwaXJhdGlvbkRhdGVUaW1lIjoiMjAyMi0wNS0xN1QwMTozOTozNS41OTQzNTRaIn0.V-XctqiwIpm0FCkCyrqRLcj1LitR8Ns_61DAarhEZio');
            $response = $client->get($url, array('headers' => $header));
            $result = $response->getBody()->getContents();
            return $result;
        }catch (RequestException $e){
            $response = $this->StatusCodeHandling($e);
            return $response;
        }
    }

    /**
     * User : Duong Hoai Sang
     * Note : Get by Id EndPoint API. Return json data.
     */
    public function getByEndPointApi($endPoint,$id){
        try{
            $client = new Client();
            $endPointUrl = 'http://199.247.16.199:1000/'.$endPoint.'/'.$id;
            $option = array('exceptions' => false);
            $header = array('Authorization'=>'Bearer ' . 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjAsIlVzZXJOYW1lIjoiYWRtaW4iLCJTY29wZXMiOlsiYWRtaW4iXSwiRXhwaXJhdGlvbkRhdGVUaW1lIjoiMjAyMi0wNS0xN1QwMTozOTozNS41OTQzNTRaIn0.V-XctqiwIpm0FCkCyrqRLcj1LitR8Ns_61DAarhEZio');
            $response = $client->get($endPointUrl, array('headers' => $header));
            $result = $response->getBody()->getContents();
            return $result;
        }catch (RequestException $e){
            $response = $this->StatusCodeHandling($e);
            return $response;
        }
    }

    /**
     * User : Duong Hoai Sang
     * Note : Post EndPoint API. Return json data.
     */
    public function postEndPointApi($endPoint,$data){
        try{
            $client = new Client();
            $endPointUrl = 'http://199.247.16.199:1000/'.$endPoint;
            $option = array('exceptions' => false);
            $header = array('Authorization'=>'Bearer ' . 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjAsIlVzZXJOYW1lIjoiYWRtaW4iLCJTY29wZXMiOlsiYWRtaW4iXSwiRXhwaXJhdGlvbkRhdGVUaW1lIjoiMjAyMi0wNS0xN1QwMTozOTozNS41OTQzNTRaIn0.V-XctqiwIpm0FCkCyrqRLcj1LitR8Ns_61DAarhEZio');
            try {
                $response = $client->request('POST', $endPointUrl, [
                    'form_params' => $data,
                    'headers'=>$header
                ]);
                return $response;
            } catch (Exception $e) {
                print_r($e->getMessage());die;
            }
            //$response = $response->getBody()->getContents();
            //echo '<pre>'.print_r($response).'</pre>';die();
            //{ "IsSuccess": true, "Code": 0, "Result": 102 }

            // echo '<pre>' . var_export($response->getStatusCode(), true) . '</pre>';
            // echo '<pre>' . var_export($response->getBody()->getContents(), true) . '</pre>';
            //echo($response->getBody()->getContents());die();

            //$response = $response->send($response);
            //$request = $client->post($url, array('headers' => $header,'body'=>$data));
            //$response = $request->send();
            //$result = $response->getBody()->getContents();

        }catch (RequestException $e){
            $response = $this->StatusCodeHandling($e);
            return $response;
        }
    }

     /**
     * User : Duong Hoai Sang
     * Note : PUT EndPoint API. Return json data.
     */
    public function putEndPointApi($endPoint,$id,$data){
        try{
            $client = new Client();
            $endPointUrl = 'http://199.247.16.199:1000/'.$endPoint.'/'.$id.'/update';
            $option = array('exceptions' => false);
            $header = array('Authorization'=>'Bearer ' . 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjAsIlVzZXJOYW1lIjoiYWRtaW4iLCJTY29wZXMiOlsiYWRtaW4iXSwiRXhwaXJhdGlvbkRhdGVUaW1lIjoiMjAyMi0wNS0xN1QwMTozOTozNS41OTQzNTRaIn0.V-XctqiwIpm0FCkCyrqRLcj1LitR8Ns_61DAarhEZio');
            try {
                $response = $client->request('POST', $endPointUrl, [
                    'form_params' => $data,
                    'headers'=>$header
                ]);
                return $response;
            } catch (Exception $e) {
                print_r($e->getMessage());die;
            }
        }catch (RequestException $e){
            $response = $this->StatusCodeHandling($e);
            return $response;
        }
    }

    /**
     * User : Duong Hoai Sang
     * Note : Delete EndPoint API.
     */
    public function deleteEndPointApi($endPoint,$id){
        try{
            $client = new Client();
            $endPointUrl = 'http://199.247.16.199:1000/'.$endPoint.'/'.$id;
            $option = array('exceptions' => false);
            $header = array('Authorization'=>'Bearer ' . 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjAsIlVzZXJOYW1lIjoiYWRtaW4iLCJTY29wZXMiOlsiYWRtaW4iXSwiRXhwaXJhdGlvbkRhdGVUaW1lIjoiMjAyMi0wNS0xN1QwMTozOTozNS41OTQzNTRaIn0.V-XctqiwIpm0FCkCyrqRLcj1LitR8Ns_61DAarhEZio');
            try {
                if($id>0){
                    $response = $client->request('DELETE', $endPointUrl, [
                        'headers'=>$header
                    ]);
                }
            } catch (Exception $e) {
                print_r($e->getMessage());die;
            }
        }catch (RequestException $e){
            $response = $this->StatusCodeHandling($e);
            return $response;
        }
    }

}
