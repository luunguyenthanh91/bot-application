<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use App\Contact_query;
use App\Payment;
use App\Report_ad;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(){
        $menu = 'home';
        // foreach ($client->getCoins() as $idx => $coin) {
        //     if($coin->getName() == 'Bitcoin'){
        //         echo "<pre>";
        //         print_r($coin);
        //
        //     }
        // }
        // die;
        // $url = 'https://pro-api.coinmarketcap.com/v1/global-metrics/quotes/historical';
        // $parameters = [
        // 'symbol' => 'BTC,ETH,NEO,BCH,XEM'
        // ];

        // $headers = [
        // 'Accepts: application/json',
        // 'X-CMC_PRO_API_KEY: 2ccf52b2-58bc-47f4-9bbd-419d80c25b07'
        // ];
        // $qs = http_build_query($parameters); // query string encode the parameters
        // $request = "{$url}?{$qs}"; // create the request URL


        // $curl = curl_init(); // Get cURL resource
        // // Set cURL options
        // curl_setopt_array($curl, array(
        // CURLOPT_URL => $request,            // set the request URL
        // CURLOPT_HTTPHEADER => $headers,     // set the headers
        // CURLOPT_RETURNTRANSFER => 1         // ask for raw response instead of bool
        // ));

        // $response = curl_exec($curl); // Send the request, save the response
        // print_r(json_decode($response)); // print json decoded response
        // curl_close($curl); // Close request
        $url = 'https://api.coinpaprika.com/v1/coins/btc-bitcoin/ohlcv/today';
        $btc = $this->getOutSide($url);
        $btc = json_decode($btc);
        $btc[0]->pacent = ($btc[0]->open - $btc[0]->close) * 100 / $btc[0]->open;
        if($btc[0]->pacent > 0){
            $btc[0]->pacent = '-'.$btc[0]->pacent;
        }else{
            $btc[0]->pacent = str_replace("-","+",$btc[0]->pacent);
        }


        $url = 'https://api.coinpaprika.com/v1/coins/eth-ethereum/ohlcv/today';
        $eth= $this->getOutSide($url);
        $eth = json_decode($eth);
        $eth[0]->pacent = ($eth[0]->open - $eth[0]->close) * 100 / $eth[0]->open;
        if($eth[0]->pacent > 0){
            $eth[0]->pacent = '-'.$eth[0]->pacent;
        }else{
            $eth[0]->pacent = str_replace("-","+",$eth[0]->pacent);
        }


        $url = 'https://api.coinpaprika.com/v1/coins/usdt-tether/ohlcv/today';
        $usdt = $this->getOutSide($url);
        $usdt = json_decode($usdt);
        $usdt[0]->pacent = ($usdt[0]->open - $usdt[0]->close) * 100 / $usdt[0]->open;
        if($usdt[0]->pacent > 0){
            $usdt[0]->pacent = '-'.$usdt[0]->pacent;
        }else{
            $usdt[0]->pacent = str_replace("-","+",$usdt[0]->pacent);
        }

        // $dateNow = date_create()->format('Y-m-d');
        // $url_dailybalances = 'dailybalances?userid=999&fromdate=2010-01-01&pagesize=2000&todate='.$dateNow;
        //
        //
        // $dataResponse = $this->getEndPointApi($url_dailybalances);
        // $dataResponse = json_decode($dataResponse);
        // $data_report = [];
        // foreach ($dataResponse->Result as $key => $value) {
        //     if(@$data_report[$value->ExchangeName]){
        //         if(@$data_report[$value->ExchangeName][$value->Currency]){
        //           $data_report[$value->ExchangeName][$value->Currency]['Available'] +=  $value->Available;
        //           $data_report[$value->ExchangeName][$value->Currency]['TotalBtc'] +=  $value->TotalBtc;
        //           $data_report[$value->ExchangeName][$value->Currency]['TotalUsdt'] +=  $value->TotalUsdt;
        //
        //         }else{
        //           $data_report[$value->ExchangeName][$value->Currency]['Available'] =  $value->Available;
        //           $data_report[$value->ExchangeName][$value->Currency]['TotalBtc'] =  $value->TotalBtc;
        //           $data_report[$value->ExchangeName][$value->Currency]['TotalUsdt'] =  $value->TotalUsdt;
        //         }
        //     }else{
        //
        //       $data_report[$value->ExchangeName] = [];
        //       $data_report[$value->ExchangeName][$value->Currency]['Available'] =  $value->Available;
        //       $data_report[$value->ExchangeName][$value->Currency]['TotalBtc'] =  $value->TotalBtc;
        //       $data_report[$value->ExchangeName][$value->Currency]['TotalUsdt'] =  $value->TotalUsdt;
        //     }
        // }
        // print_r($data_report);die;
        return view('admin.pages.dashboard',compact('usdt','eth','btc','menu'));
    }


    public function logout(){
        if (Auth::check()){
            Auth::logout();
        }
        return redirect('/admin/login');
    }
}
