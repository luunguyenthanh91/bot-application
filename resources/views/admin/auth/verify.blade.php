@extends('layouts.admin_login')

@section('content')
    <div class="page-single" style="width: 100%;justify-content: flex-end;">

      <img src="{{ asset('admins/assets/logo_bot.png') }}" class="logo_login" alt="Spain logo">
      <div class="bg_login">
        <div class="row">
          <div class="col mx-auto">

            <div class="row justify-content-center">
              <div class="col-md-8" style="    background: none;">
                <div class="card-group mb-0" style="    background: none;">


                  <div class="card form_login_style"  style="background:url({{ asset('admins/assets/verify.png') }})  no-repeat  ;">
                    <div class="card-body">
                      
                      @if(@$message)
                          <div class="alert_form_login alert alert-danger col-md-12">
                              <h4 class="col-md-12 col-12">- {{$message}}</h4>
                          </div>
                      @endif

                      <form method="post" class="form_login_style_all"  action="/admin/verify">
                          @csrf
                          <input type="text"  required name="code" class=" email_form_login  form-control" placeholder="Code">
                          <button type="submit" class=" btn_form_login btn btn-gradient-primary btn-block">Confirm</button>
                      </form>


                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('page-js')

@endsection
