@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container">

    <div class="page-header">
      <h4 class="page-title">Thành Viên</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Quản Lý Thành Viên</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>

          <div class="row row-cards">
							<div class="col-lg-3">
								<div class="row">
									<div class="col-md-12 col-lg-12">
										<div class="card">
											<div class="card-header">
												<div class="card-title"> Điều Kiện Lọc</div>
											</div>
											<div class="card-body">
												<div class="form-group">
													<label class="form-label">Kiểu Đăng Nhập</label>
													<select id="type_login" name="condition[type_login]" class="form-control custom-select">
														<option value="">--Tất Cả--</option>
														<option value="1" @if(@$condition['type_login'] == 1 ) selected @endif>Website</option>
														<option value="2" @if(@$condition['type_login'] == 2 ) selected @endif>Facebook</option>
													</select>
												</div>
												<div class="form-group">
													<label class="form-label">Số Điện Thoại</label>
                          <input type="text" id="phone" name="phone" value="{{@$condition['phone']}}" class="form-control custom-select" placeholder="Số Điện Thoại">
                          <button type="button" class="btn btn-primary btn_search col-md-12 col-lg-12  mt-3">
                            <i class="fa fa-search " aria-hidden="true"></i>
                          </button>
												</div>


                        <div class="form-group">
                            <label class="form-label">Thêm Thành Viên</label>
        										<a href="/admin/add-user" class="btn btn-primary  col-md-12 col-lg-12">
        											<i class="mdi mdi-account-plus " aria-hidden="true"></i>
        										</a>
												</div>
                        <form method="post" action="/admin/import-excel"  enctype="multipart/form-data">
                            @csrf
    												<label class="form-label">Import Excel</label>
    												<div class="input-group">
                              <input required type="file" name="file_excel" class="form-control br-tl-7 br-bl-7" placeholder="Email">
            									<div class="input-group-append ">
            										<button type="submit" class="btn btn-primary br-tr-7 br-br-7">
            											<i class="mdi mdi-arrow-expand-up " aria-hidden="true"></i>
            										</button>
            									</div>
    												</div>
                        </form>

                        <form method="post" action="" id="form_search"  >
                            @csrf
                            <input type="hidden" name="condition[email]" id="email_val" />
                            <input type="hidden" name="condition[phone]"  id="phone_val" />
                            <input type="hidden" name="condition[type_login]"  id="type_login_val" />
                        </form>


											</div>
										</div>

									</div>
								</div>
							</div>

							<div class="col-lg-9">

								<div class="input-group">
									<input type="text" name="email" id="email" value="{{@$condition['email']}}" class="form-control br-tl-7 br-bl-7" placeholder="Email">
									<div class="input-group-append ">
										<button type="button" class="btn btn_search btn-primary br-tr-7 br-br-7">
											<i class="fa fa-search " aria-hidden="true"></i>
										</button>
									</div>

								</div>

								<div class="card mt-5 store">
                  @if($message != '')
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$message}}
                  </div>
                  @endif
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th  class="text-center">
                                #
                            </th>
                            <th class="text-center">Họ Tên</th>
                            <th>Địa Chỉ</th>
                            <th>Email</th>
                            <th>Số Điện Thoại</th>
                            <th>Kiểu Thành Viên</th>
                            <th class="text-center"></th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($users as $item)
                              <tr>
                                <td class="align-middle text-center">
                                    {{$item->id}}
                                </td>
                                <td class="align-middle text-center">{{$item->name}}</td>
                                <td class="text-nowrap align-middle">{{$item->address}}</td>
                                <td class="text-nowrap align-middle">{{$item->email}}</td>
                                <td class="text-nowrap align-middle">{{$item->phone}}</td>
                                <td class="text-nowrap align-middle">@if( @$item->type_login == '1') Website @else Facebook @endif</td>
                                <td class="text-center align-middle">
                                  <div class="btn-group align-top">
                                    <a class="btn btn-sm btn-primary badge" href="/admin/edit-user/{{$item->id}}" ><i class="mdi mdi-account-edit"></i>  </a>
                                    <button class="btn btn-sm btn-primary badge delete_conform" id="{{$item->id}}" type="button"><i class="fa fa-trash"></i></button>
                                  </div>
                                </td>

                              </tr>
                         @endforeach


                        </tbody>
                      </table>
                    </div>

                    {{ $users->appends(request()->query())->links() }}

                  </div>

                </div>
							</div>
						</div>


        </div>
      </div>

    </div>

  </div>
  <!--footer-->


  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <!-- <script src="{{ asset('admins/assets/js/index3.js') }}"></script> -->
    <!-- <script src="{{ asset('admins/assets/js/index3-worldmap.js') }}"></script> -->
    <link href="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet" />
    <script src="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('admins/assets/js/sweet-alert.js') }}"></script>
    <script>

        $( document ).ready(function() {
            $('.btn_search').on("click",function(){
                var email = $("#email").val();
                var phone = $("#phone").val();
                var type_login = $("#type_login").val();
                $("#type_login_val").val(type_login);
                $("#phone_val").val(phone);
                $("#email_val").val(email);
                $("#form_search").submit();
            });
            $(".delete_conform").on("click",function(){
                var id = $(this).attr('id');
                swal({
            			title: "Xác nhận",
            			text: "Bạn đồng ý xoá thành viên này ?",
            			type: "warning",
            			showCancelButton: true,
            			confirmButtonText: 'Có',
            			cancelButtonText: 'Không'
            		}, function(inputValue) {
                    if(inputValue){
                      var url= "/admin/delete-user/"+id;
                      window.location = url;
                    }

            		});
            });
        });
    </script>
@endsection
