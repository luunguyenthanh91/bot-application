<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

/**
 * User : Duong Hoai Sang
 * Note : ExchangesController
 */
class ExchangesController extends Controller
{

    /**
     * User : Duong Hoai Sang.
     * Note : Show list exchanges.
     */
    public function showListExchanges(){
        $menu = 'exchanges';
        $message = "";
        return view('admin.exchanges.getlistexchanges', compact('message','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     */
    public function getListExchanges(){
        $url = 'exchanges';
        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }

}
