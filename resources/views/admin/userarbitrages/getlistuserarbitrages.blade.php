@extends('layouts.admin_main')

@section('content')
<link href="{{ asset('admins/assets/plugins/select2/select2.min.css') }}" rel="stylesheet" />
<style>
.select2-search__field{
    color: #fff !important;
}
</style>
<div class="content-area">
  <div class="container-fluid">

    <div class="page-header">
      <h4 class="page-title">Userarbitrages</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Userarbitrages</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>
          <div class="row row-cards">

              <div class="col-lg-3">
                <div class="row">

                  <div class="col-md-12 col-lg-12">
                    <div class="card">

                      <div class="card-header">
                        <div class="card-title"> Search</div>
                      </div>

                      <div class="card-body">

                        <div class="form-group col-xl-12 col-md-12 float-left">
                          <label class="form-label">Base Currency</label>

                            <input id="event_id" value="{{@$condition['event_id']}}" name="condition[event_id]" type="text" class="form-control custom-select col-xl-12 col-md-12 ">
                          <button type="button" class="btn btn-primary btn_search col-xl-12 col-md-12 col-lg-12">
                            <i class="fa fa-search " aria-hidden="true"></i>
                          </button>
                        </div>

                        <div class="form-group col-xl-12 col-md-12 float-left">
                            <label class="form-label">Add Data</label>
                            <a href="/admin/userarbitrages/add" class="btn btn-primary col-xl-12 col-md-12 col-lg-12">
                              <i class="fa fa-calendar-plus-o " aria-hidden="true"></i>
                            </a>
                        </div>


                        <form method="post" action="" id="form_search">
                            @csrf
                            <input type="hidden" name="condition[event_id]" id="event_id_val">
                        </form>


                      </div>

                    </div>

                  </div>
                </div>
              </div>
<!--
              <div class="col-lg-9">
                <div class="float-right">
                  <a href="/admin/userarbitrages/add" class="btn btn-primary col-xl-1 col-md-1 col-lg-1 float-right">
                    Add Data <i class="fa fa-calendar-plus-o " aria-hidden="true"></i>
                  </a>
                </div>
              </div> -->

							<div class="col-lg-9">
								<div class="card store">
                  @if($message != '')
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$message}}
                  </div>
                  @endif
                  <div class="update_status" style="display:none">
                    <div class="alert alert-success" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Update status success.
                    </div>
                  </div>
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table id="userarbitrages" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                          <tr>
                            <th class="text-center">Market Name</th>
                            <th class="text-center">Base Currency</th>
                            <th class="text-center">Currency</th>
                            <th class="text-center">Min Profit Pct</th>
                            <th class="text-center">Active</th>
                            <th class="text-center">Execute Signal</th>
                            <!-- <th class="text-center">On / Off</th> -->
                            <th class="text-center">Action</th>
                          </tr>
                        </thead>
                        <tbody class="row-data-load-daily-balances">
                        <!-- Load data tai day -->
                        </tbody>
                      </table>
                      <div class="card-body">
    										<div class="pagination-wrapper">
    											<nav aria-label="Page navigation">
    												<ul class="pagination mb-0" id="show_page">



    												</ul>
    											</nav>
    										</div>
    										<!-- pagination-wrapper -->
    									</div>
                    </div>
                  </div>
                </div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->

  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <link href="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet" />
    <script src="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('admins/assets/js/sweet-alert.js') }}"></script>


    <script src="{{ asset('admins/assets/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/time-picker/jquery.timepicker.js') }}"></script>
		<script src="{{ asset('admins/assets/plugins/time-picker/toggles.min.js') }}"></script>

    <script src="{{ asset('admins/assets/plugins/date-picker/spectrum.js') }}"></script>
		<script src="{{ asset('admins/assets/plugins/date-picker/jquery-ui.js') }}"></script>
		<script src="{{ asset('admins/assets/plugins/input-mask/jquery.maskedinput.js') }}"></script>

    <script src="{{ asset('admins/assets/js/select2.js') }}"></script>

    <script>
      var page_index = 1;
      $( document ).ready(function() {
        $("body").on("click",".delete_conform",function(){
            var id = $(this).attr('id');
            swal({
              title: "Xác nhận",
              text: "Bạn đồng ý xoá?",
              type: "warning",
              showCancelButton: true,
              confirmButtonText: 'Có',
              cancelButtonText: 'Không'
            }, function(inputValue) {
                if(inputValue){
                  var url= "/admin/userarbitrages/delete/"+id;
                  window.location = url;
                }

            });
        });
      });
    </script>

    <script type="text/javascript">
        function loadPage(page_flag){
          page_index = page_flag;
          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '/admin/userarbitrages/get-list-userarbitrages?page='+page_index,
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj.TotalRecord);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='';
                if(parseInt(obj.TotalPageCount) > 1){
                    html_page ='';
                    for(var i = 1 ; i <= parseInt(obj.TotalPageCount); i++){
                        if(page_index == i ){
                            html_page += '<li class="page-item active">';
                        }else{
                            html_page += '<li class="page-item">';
                        }

                        html_page += '<a class="page-link" onclick="loadPage('+i+')">'+i+'</a> </li>';
                    }
                    $('#show_page').html(html_page);
                }
                arraydata.forEach(function(object) {
                  //console.log(object.ExchangeName);
                  valueColumn+= "<tr><td class='align-middle text-center'>"+object.MarketName+"</td>"+
                  "<td class='align-middle text-center'>"+object.BaseCurrency+"</td>"+
                  "<td class='align-middle text-center'>"+object.Currency+"</td>"+
                  "<td class='align-middle text-center'>"+object.MinProfitPct+"</td>"+
                  // "<td class='align-middle text-center'>"+object.IsActive+"</td>"+
                  '<td class="align-middle" style="justify-content: center; align-items: center; ">'+
                    '<div class="material-switch pull-right">';
                      if(object.IsActive == true){
                        valueColumn+=   '<input id="check_'+object.Id+'" onchange="toggleChange('+object.Id+');"  checked  type="checkbox" type="checkbox"/>';
                      }else{
                        valueColumn+=   '<input id="check_'+object.Id+'" onchange="toggleChange('+object.Id+');"  type="checkbox" type="checkbox"/>';
                      }
                      valueColumn+=    '<label for="check_'+object.Id+'" class="label-success"></label>'+
                    '</div>'+
                  '</td>'+

                  // "<td class='align-middle text-center'>"+object.IsExecuteSignal+"</td>"+
                  '<td class="align-middle" style="justify-content: center; align-items: center;">'+
                    '<div class="material-switch pull-right">';
                      if(object.IsExecuteSignal == true){
                        valueColumn+=   '<input id="check_exe_'+object.Id+'" value="'+object.MarketName+'" onchange="toggleChangeExe('+object.Id+');"  checked  type="checkbox" type="checkbox"/>';
                      }else{
                        valueColumn+=   '<input id="check_exe_'+object.Id+'" value="'+object.MarketName+'" onchange="toggleChangeExe('+object.Id+');"  type="checkbox" type="checkbox"/>';
                      }
                      valueColumn+=  '<label for="check_exe_'+object.Id+'" class="label-success"></label>'+
                    '</div>'+
                  '</td>'+

                  '<td class="align-middle text-center"><a class="btn btn-info btn-sm" href="userarbitrages/edit/'+object.Id+'"><i class="mdi mdi-account-edit"></i></a>' + '<a class="btn btn-danger btn-sm delete_conform" id='+object.Id+'><i class="fa fa-trash"></i></a></td>'+
                  "</tr>"
                });
                $(".row-data-load-daily-balances").html(valueColumn);

                // $('#userarbitrages').DataTable({
                //   data:  arraydata,
                //   "pageLength": 25,
                //   columns: [
                //     { data: 'MarketName' },
                //     { data: 'BaseCurrency' },
                //     { data: 'Currency' },
                //     { data: 'MinProfitPct' },
                //     { data: 'IsActive' },
                //     { data: 'IsExecuteSignal' }
                //   ]
                // });

              },error:function(){
                  console.log(data);
              }
          });
        }
        $( document ).ready(function() {
          // setInterval(function() {
          // }, 5000);


          // $.ajax({
          //     type: 'GET', //THIS NEEDS TO BE GET
          //     url: '{{route("get_list_markets")}}',
          //     dataType: 'json',
          //     success: function (data) {
          //       //var obj = JSON.parse(data.data);
          //       //console.log(obj);
          //       var obj = JSON.parse(data.data);
          //       var arraydata = obj.Result;
          //       var valueColumn='<option value="">--All--</option>';
          //       var flag = '';
          //       arraydata.forEach(function(object) {
          //
          //         if(('{{@$condition['event_id']}}') == object.MarketName){
          //             valueColumn+= '<option value="'+object.MarketName+'" checked>'+object.MarketName+'</option>';
          //             flag = object.MarketName;
          //         }else{
          //             valueColumn+= '<option value="'+object.MarketName+'">'+object.MarketName+'</option>'
          //         }
          //
          //       });
          //
          //       $("#event_id").html(valueColumn);
          //       if(flag != ''){
          //         $("#event_id option[value='"+flag+"']").attr('selected', 'selected');
          //       }
          //     },error:function(){
          //         console.log(data);
          //     }
          // });

          $('.btn_search').on("click",function(){
              var event_id = $("#event_id").val();
              $("#event_id_val").val(event_id);
              $("#form_search").submit();
          });


          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '/admin/userarbitrages/get-list-userarbitrages?page='+page_index,
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj.TotalRecord);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='';
                console.log(obj.TotalPageCount);
                if(parseInt(obj.TotalPageCount) > 1){
                    html_page ='';
                    for(var i = 1 ; i <= parseInt(obj.TotalPageCount); i++){
                        if(page_index == i ){
                            html_page += '<li class="page-item active">';
                        }else{
                            html_page += '<li class="page-item">';
                        }

                        html_page += '<a class="page-link" onclick="loadPage('+i+')">'+i+'</a> </li>';
                    }
                    $('#show_page').html(html_page);
                }
                arraydata.forEach(function(object) {
                  //console.log(object.ExchangeName);
                  valueColumn+= "<tr><td class='align-middle text-center'>"+object.MarketName+"</td>"+
                  "<td class='align-middle text-center'>"+object.BaseCurrency+"</td>"+
                  "<td class='align-middle text-center'>"+object.Currency+"</td>"+
                  "<td class='align-middle text-center'>"+object.MinProfitPct+"</td>"+
                  // "<td class='align-middle text-center'>"+object.IsActive+"</td>"+
                  '<td class="align-middle" style="justify-content: center; align-items: center; ">'+
                    '<div class="material-switch pull-right">';
                      if(object.IsActive == true){
                        valueColumn+=   '<input id="check_'+object.Id+'" onchange="toggleChange('+object.Id+');"  checked  type="checkbox" type="checkbox"/>';
                      }else{
                        valueColumn+=   '<input id="check_'+object.Id+'" onchange="toggleChange('+object.Id+');"  type="checkbox" type="checkbox"/>';
                      }
                      valueColumn+=    '<label for="check_'+object.Id+'" class="label-success"></label>'+
                    '</div>'+
                  '</td>'+

                  // "<td class='align-middle text-center'>"+object.IsExecuteSignal+"</td>"+
                  '<td class="align-middle" style="justify-content: center; align-items: center;">'+
                    '<div class="material-switch pull-right">';
                      if(object.IsExecuteSignal == true){
                        valueColumn+=   '<input id="check_exe_'+object.Id+'" value="'+object.MarketName+'" onchange="toggleChangeExe('+object.Id+');"  checked  type="checkbox" type="checkbox"/>';
                      }else{
                        valueColumn+=   '<input id="check_exe_'+object.Id+'" value="'+object.MarketName+'" onchange="toggleChangeExe('+object.Id+');"  type="checkbox" type="checkbox"/>';
                      }
                      valueColumn+=  '<label for="check_exe_'+object.Id+'" class="label-success"></label>'+
                    '</div>'+
                  '</td>'+

                  '<td class="align-middle text-center"><a class="btn btn-info btn-sm" href="userarbitrages/edit/'+object.Id+'"><i class="mdi mdi-account-edit"></i></a>' + '<a class="btn btn-danger btn-sm delete_conform" id='+object.Id+'><i class="fa fa-trash"></i></a></td>'+
                  "</tr>"
                });
                $(".row-data-load-daily-balances").html(valueColumn);

                // $('#userarbitrages').DataTable({
                //   data:  arraydata,
                //   "pageLength": 25,
                //   columns: [
                //     { data: 'MarketName' },
                //     { data: 'BaseCurrency' },
                //     { data: 'Currency' },
                //     { data: 'MinProfitPct' },
                //     { data: 'IsActive' },
                //     { data: 'IsExecuteSignal' }
                //   ]
                // });

              },error:function(){
                  console.log(data);
              }
          });
        });
    </script>

    <script>


        function toggleChange(id){
            console.log();
            if($('#check_'+id).is(":checked") == true){
                $.ajax({
                   url: "/admin/userarbitrages/start-trade/"+id,
                   type: "get",
                   success: function (response) {
                     $(".update_status").attr("style","display: block;");
                   }
               });
            }else{
                $.ajax({
                   url: "/admin/userarbitrages/stop-trade/"+id,
                   type: "get",
                   success: function (response) {
                     $(".update_status").attr("style","display: block;");
                   }
               });
            }
        }
        function toggleChangeExe(id){

    $.ajaxSetup({

      headers: {

          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

      }

      });
            if($('#check_exe_'+id).is(":checked") == true){
                var marketname=document.getElementById("check_exe_"+id).value;
                $.ajax({

                   url: "/admin/userarbitrages/update-execute-signal",
                   type: "post",
                   dataType:"json", // dữ liệu trả về dạng text
                   data : { // Danh sách các thuộc tính sẽ gửi đi
                    IsExecuteSignal : "true",  id : id, marketname : marketname
                   },
                   success: function (data) {
                      $(".update_status").attr("style","display: block;");
                   }
               });
            }else{
              var marketname=document.getElementById("check_exe_"+id).value;
                $.ajax({
                   url: "/admin/userarbitrages/update-execute-signal",
                   type: "post",
                   dataType:"json", // dữ liệu trả về dạng text
                   data : { // Danh sách các thuộc tính sẽ gửi đi
                    IsExecuteSignal : "false", id : id, marketname : marketname
                   },
                   success: function (data) {
                      $(".update_status").attr("style","display: block;");
                   }
               });
            }
        }
    </script>

@endsection
