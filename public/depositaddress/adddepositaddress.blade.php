@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container">

    <div class="page-header">
      <h4 class="page-title">Depositaddress</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item"><a href="/admin/depositaddress">Manager Depositaddress</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add Depositaddress</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
          <form action="" method="post">
            @csrf
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0 card-title">Depositaddress Info</h3>
              </div>
              <div class="card-body">

                @if(@$message != '')
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <p>- {{$message}}</p>
                </div>
                @endif

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="data[Address]" placeholder="Address" value="{{@$data['Address']}}">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" name="data[AddressTag]" placeholder="AddressTag" value="{{@$data['AddressTag']}}" >
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group has-success">
                        <select id="event_id" required name="data[ExchangeId]" class="form-control custom-select">
                        </select>
                    </div>

                    <div class="form-group">
                      <input type="text" required class="form-control" name="data[Currency]" placeholder="Currency" value="{{@$data['Currency']}}" >
                    </div>
                    <!-- <div class="form-group  has-danger">
                      <input type="number" required class="form-control" name="data[Id]" placeholder="Id" value="{{@$data['Id']}}">
                    </div> -->
                    <!-- <div class="form-group">
                      <input type="number" required class="form-control" name="data[UserId]" placeholder="UserId" value="{{@$data['UserId']}}">
                    </div> -->
                  </div>

                </div>
              </div>
              <div class="card-footer text-right">
    						<button type="submit" class="btn btn-primary">Add Depositaddress</button>
    					</div>

            </div>
        </form>
      </div>

    </div>

  </div>
  <!--footer-->


  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
<script type="text/javascript">

    $( document ).ready(function() {
      // setInterval(function() {
      // }, 5000);


      $.ajax({
          type: 'GET', //THIS NEEDS TO BE GET
          url: '{{route("get_list_exchanges")}}',
          dataType: 'json',
          success: function (data) {
            //var obj = JSON.parse(data.data);
            //console.log(obj);
            var obj = JSON.parse(data.data);
            var arraydata = obj.Result;
            var valueColumn='';

            arraydata.forEach(function(object) {
              //Check URL website.
              if(object.WebUrl=='' || object.WebUrl == undefined){
                webUrl = '...';
              }else{
                webUrl = object.WebUrl;
              }
              if(({{@$data['ExchangeId']}} + '') == object.Id){
                  valueColumn+= '<option value="'+object.Id+'" checked>'+object.Name+'</option>';
              }else{
                  valueColumn+= '<option value="'+object.Id+'">'+object.Name+'</option>'
              }

            });
            $("#event_id").html(valueColumn);
          },error:function(){
              console.log(data);
          }
      });


    });
</script>

    <!-- <script>

        $( document ).ready(function() {
            $('.btn_search').on("click",function(){
                var email = $("#email").val();
                var phone = $("#phone").val();
                var type_login = $("#type_login").val();
                $("#type_login_val").val(type_login);
                $("#phone_val").val(phone);
                $("#email_val").val(email);
                $("#form_search").submit();
            });
            $(".delete_conform").on("click",function(){
                var id = $(this).attr('id');
                swal({
            			title: "Xác nhận",
            			text: "Bạn đồng ý xoá thành viên này ?",
            			type: "warning",
            			showCancelButton: true,
            			confirmButtonText: 'Có',
            			cancelButtonText: 'Không'
            		}, function(inputValue) {
                    if(inputValue){
                      var url= "/admin/delete-user/"+id;
                      window.location = url;
                    }

            		});
            });
        });
    </script> -->
@endsection
