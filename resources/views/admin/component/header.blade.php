<div class="app-header header py-1 d-flex">
  <div class="container">
    <div class="d-flex">
      <a class="header-brand" href="">
        <img src="{{ asset('admins/assets/logo_bot.png') }}" class="header-brand-img d-none d-sm-block" alt="Spain logo">
        <img src="{{ asset('admins/assets/logo_bot.png') }}" class="header-brand-img-2 d-sm-none" alt="Spain logo">
      </a>
      <a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>


        <div class="d-flex order-lg-2 ml-auto">


        <div class="dropdown d-none d-md-flex " >
          <a  class="nav-link icon full-screen-link">
            <i class="mdi mdi-arrow-expand-all"  id="fullscreen-button"></i>
          </a>
        </div>

        <div class="dropdown d-none d-md-flex">
          <a class="nav-link icon" data-toggle="dropdown">
            <i class="fe fe-grid floating"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow p-0">
            <ul class="drop-icon-wrap p-0 m-0">
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-mail"></i>
                  <span class="block"> E-mail</span>
                </a>
              </li>
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-calendar"></i>
                  <span class="block">calendar</span>
                </a>
              </li>
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-map-pin"></i>
                  <span class="block">map</span>
                </a>
              </li>
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-shopping-cart"></i>
                  <span class="block">Cart</span>
                </a>
              </li>
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-message-square"></i>
                  <span class="block">chat</span>
                </a>
              </li>
              <li>
                <a href="#" class="drop-icon-item">
                  <i class="fe fe-phone-outgoing"></i>
                  <span class="block">contact</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="dropdown">
          <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
            <span class="avatar avatar-md brround"><img src="https://static05.cminds.com/wp-content/uploads/computer-1331579_1280-1-300x300.png" alt="Profile-img" class="avatar avatar-md brround"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow ">

            <a class="dropdown-item" href="/admin/logout">
              <i class="dropdown-icon mdi  mdi-logout-variant"></i> Sign out
            </a>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Horizontal-menu-->
<!-- <div class="horizontal-main clearfix">
  <div class="horizontal-mainwrapper container clearfix">
    <nav class="horizontalMenu clearfix">
      <ul class="horizontalMenu-list">
        <li>
          <a href="/admin" class="sub-icon @if( @$menu == 'home') active @endif">
            <i class="fa fa-home"></i> Trang Chủ
          </a>
        </li>
        <li>
          <a href="/admin/dailybalances" class="sub-icon @if( @$menu == 'daily-balances') active @endif">
            <i class="fa fa-balance-scale"></i> Daily Balances
          </a>
        </li>

        <li>
          <a href="/admin/dailybalances/exchangesummaries" class="sub-icon @if( @$menu == 'daily-balances-exchangesummaries') active @endif">
            <i class="fa fa-exchange"></i> Exchangesummaries
          </a>
        </li>       -->

        <!-- <li>
          <a href="/admin/users" class="sub-icon @if( @$menu == 'users') active @endif">
            <i class="si si-user"></i> Thành Viên
          </a>
        </li>
        <li>
          <a href="#" class="sub-icon @if( @$menu == 'events') active @endif">
            <i class="si si-game-controller"></i> Sự Kiện
          </a>
        </li>
        <li>
          <a href="#" class="sub-icon @if( @$menu == 'gifts') active @endif">
            <i class="si si-present"></i> Giải Thưởng
          </a>
        </li>
        <li>
          <a href="#" class="sub-icon @if( @$menu == 'historys') active @endif">
            <i class="si si-notebook"></i> Lịch Sử Quay Thưởng
          </a>
        </li>
        <li>
          <a href="#" class="sub-icon @if( @$menu == 'historys_gift') active @endif">
            <i class="si si-trophy"></i> Danh Sách Trúng Thưởng
          </a>
        </li> -->


      <!-- </ul>
    </nav> -->
    <!--Menu HTML Code-->
  <!-- </div>
</div> -->


<!--Horizontal-menu-->
<div class="horizontal-main clearfix">
  <div class="horizontal-mainwrapper container clearfix">
    <nav class="horizontalMenu clearfix">
      <ul class="horizontalMenu-list">
        <li aria-haspopup="true">
          <a href="/admin" class="sub-icon @if( @$menu == 'home') active @endif">
            <i class="fa fa-home"></i> Trang Chủ
          </a>
        </li>
        <li aria-haspopup="true">
          <a href="/admin/allocatecapitals" class="sub-icon @if( @$menu == 'allocatecapitals') active @endif">
            <i class="fa fa-money"></i> Allocatecapitals
          </a>

        </li>

        <li aria-haspopup="true">
          <a href="#" class="sub-icon @if( @$menu == 'userarbitrages' || @$menu == 'depositaddress' || @$menu == 'markets' || @$menu == 'exchanges') active @endif">
              <i class="fa fa-cog"></i> System Settings  <i class="fa fa-angle-down horizontal-icon"></i>
          </a>
          <ul class="sub-menu">

              <li aria-haspopup="true">
                <a href="/admin/userarbitrages" class="sub-icon">
                  <i class="fa fa-ravelry"></i>Userarbitrages
                </a>
              </li>

              <li aria-haspopup="true">
                <a href="/admin/exchanges" class="sub-icon">
                  <i class="fa fa-stack-exchange"></i> Exchanges
                </a>
              </li>


              <li aria-haspopup="true">
                <a href="/admin/depositaddress" class="sub-icon">
                  <i class="fa fa-address-card"></i>Depositaddress
                </a>
              </li>


              <li aria-haspopup="true">
                <a href="/admin/markets" class="sub-icon">
                  <i class="fa fa-thumb-tack"></i>Market
                </a>
              </li>


          </ul>
        </li>

        <li aria-haspopup="true"><a href="#" class="sub-icon @if( @$menu == 'daily-balances') active @endif"><i class="fa fa-bandcamp"></i> Daily balances  <i class="fa fa-angle-down horizontal-icon"></i></a>
          <ul class="sub-menu">
            <li aria-haspopup="true">
              <a href="/admin/dailybalances/total-daily" class="sub-icon">
                <i class="fa fa-balance-scale"></i>Total Daily Balances
              </a>
            </li>
            <li aria-haspopup="true">
              <a href="/admin/dailybalances" class="sub-icon">
                <i class="fa fa-balance-scale"></i>List Daily Balances
              </a>
            </li>
            <li aria-haspopup="true">
              <a href="/admin/dailybalances/exchangesummaries" class="sub-icon @if( @$menu == 'daily-balances-exchangesummaries') active @endif">
                <i class="fa fa-exchange"></i> Exchangesummaries
              </a>
            </li>
            <li aria-haspopup="true">
              <a href="/admin/dailybalances/summaries" class="sub-icon @if( @$menu == 'daily-balances-summaries') active @endif">
                <i class="fa fa-exchange"></i> Summaries
              </a>
            </li>
            <li aria-haspopup="true">
              <a href="/admin/dailybalances/dailyprofit" class="sub-icon @if( @$menu == 'daily-balances-dailyprofit') active @endif">
                <i class="fa fa-exchange"></i> Dailyprofit
              </a>
            </li>
          </ul>
        </li>


        <li aria-haspopup="true"><a href="#" class="sub-icon @if( @$menu == 'arbitragetransactions' || @$menu == 'arbitragesignals') active @endif"><i class="fa fa-bar-chart-o"></i> Signal  <i class="fa fa-angle-down horizontal-icon"></i></a>
          <ul class="sub-menu">
            <li aria-haspopup="true">
              <a href="/admin/arbitragesignals" class="sub-icon">
                <i class="fa fa-bar-chart-o"></i>Arbitragesignals
              </a>
            </li>
            <li aria-haspopup="true">
              <a href="/admin/arbitragetransactions" class="sub-icon">
                <i class="fa fa-bar-chart-o"></i>Arbitragetransactions
              </a>
            </li>

          </ul>
        </li>









        <!-- <li aria-haspopup="true"><a href="" class=""><i class="fa fa-question-circle"></i> Help & Support</a></li> -->

      </ul>
    </nav>
    <!--Menu HTML Code-->
  </div>
</div>
