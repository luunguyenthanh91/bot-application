@extends('layouts.admin_main')

@section('content')
<link href="{{ asset('admins/assets/plugins/select2/select2.min.css') }}" rel="stylesheet" />
<style>
.select2-search__field{
    color: #fff !important;
}
</style>

<div class="content-area">
  <div class="container">

    <div class="page-header">
      <h4 class="page-title">Userarbitrages</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item"><a href="/admin/userarbitrages">Manager Userarbitrages</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit Userarbitrages</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
          <form action="" method="post">
            @csrf
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0 card-title">Userarbitrages Info</h3>
              </div>
              <div class="card-body">

                @if(@$message != '')
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <p>- {{$message}}</p>
                </div>
                @endif

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <p>IsExecuteSignal</p>
                      <div class="material-switch form-control">
                        <input id="check1" name="data[IsExecuteSignal]" @if( @$data['IsExecuteSignal'] == 'true') checked  @endif type="checkbox"/>
                        <label for="check1" class="label-success"></label>
                      </div>

                    </div>
                    <div class="form-group">
                      <p>MarketName</p>
                      <select id="event_id" required name="data[MarketName]" class="form-control custom-select select2-show-search">
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group has-success">

                      <p>MinProfitPct</p>
                      <input type="text" class="form-control" name="data[MinProfitPct]" placeholder="MinProfitPct" value="{{@$data['MinProfitPct']}}">
                    </div>

                    <div class="form-group">

                      <p>Note</p>
                      <input type="text" class="form-control" name="data[Note]" placeholder="Note" value="{{@$data['Note']}}" >
                    </div>
                    <div class="form-group">

                      <p>Code Confirm</p>
                      <input type="text" class="form-control" name="code" placeholder="Code Confirm" value="" >
                    </div>

                  </div>

                </div>
              </div>
              <div class="card-footer text-right">
    						<button type="submit" class="btn btn-primary">Edit</button>
    					</div>

            </div>
        </form>
      </div>

    </div>

  </div>
  <!--footer-->


  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')



    <script src="{{ asset('admins/assets/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/time-picker/jquery.timepicker.js') }}"></script>
		<script src="{{ asset('admins/assets/plugins/time-picker/toggles.min.js') }}"></script>

    <script src="{{ asset('admins/assets/plugins/date-picker/spectrum.js') }}"></script>
		<script src="{{ asset('admins/assets/plugins/date-picker/jquery-ui.js') }}"></script>
		<script src="{{ asset('admins/assets/plugins/input-mask/jquery.maskedinput.js') }}"></script>

    <script src="{{ asset('admins/assets/js/select2.js') }}"></script>
<script type="text/javascript">

    $( document ).ready(function() {
      // setInterval(function() {
      // }, 5000);


      $.ajax({
          type: 'GET', //THIS NEEDS TO BE GET
          url: '{{route("get_list_markets")}}',
          dataType: 'json',
          success: function (data) {
            //var obj = JSON.parse(data.data);
            //console.log(obj);
            var obj = JSON.parse(data.data);
            var arraydata = obj.Result;
            var valueColumn='';
            var flag = '';
            arraydata.forEach(function(object) {

              if(('{{@$data['MarketName']}}') == object.MarketName){
                  valueColumn+= '<option value="'+object.MarketName+'" checked>'+object.MarketName+'</option>';
                  flag = object.MarketName;
              }else{
                  valueColumn+= '<option value="'+object.MarketName+'">'+object.MarketName+'</option>'
              }

            });
            $("#event_id").html(valueColumn);
            if(flag != ''){
              $("#event_id option[value='"+flag+"']").attr('selected', 'selected');
            }
          },error:function(){
              console.log(data);
          }
      });


    });
</script>
@endsection
