@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container-fluid">

    <div class="page-header">
      <h4 class="page-title">Arbitragetransactions</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Arbitragetransactions</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>
          <div class="row row-cards">
							<div class="col-lg-12">
								<div class="card mt-5 store">
                  @if($message != '')
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$message}}
                  </div>
                  @endif
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table class="table table-bordered">
                        <thead>
                          <tr>

                            <th class="text-center">ArbitrageSignalId</th>
                            <th class="text-center">Sàn Mua : Giá Mua</th>
                            <th class="text-center">Sàn Bán : Giá Bán</th>
                            <th class="text-center">MarketName</th>
                            <th class="text-center">BaseCurrencyQty</th>
                            <th class="text-center">CurrencyQty</th>
                            <th class="text-center">ProfitAmt</th>
                          </tr>
                        </thead>
                        <tbody id="myList" class="row-data-load-daily-balances">
                        <!-- Load data tai day -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->

  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <script type="text/javascript">
        $( document ).ready(function() {
          console.log('batdau');
          loadRealtimeArbitragesignals();
          setInterval(function() {
            console.log('realtime');
            loadRealtimeArbitragesignals();
          }, 5000);
        });
        function loadRealtimeArbitragesignals() {
          // setInterval(function() {
          // }, 5000);

          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_list_arbitragetransactions")}}',
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='';
                arraydata.forEach(function(object) {
                  //console.log(object.ExchangeName);
                  if (!object.BuyRemark){
                      object.BuyRemark = 'null';
                  }
                  if (!object.Remark){
                      object.Remark = 'null';
                  }
                  if (!object.SellRemark){
                      object.SellRemark = 'null';
                  }
                  valueColumn+= "<tr>"+
                  "<td class='align-middle text-center'>"+object.ArbitrageSignalId+"</td>"+
                  "<td class='align-middle text-center'>"+object.BuyExchangeName+" : "+object.BuyPrice+"</td>"+
                  "<td class='align-middle text-center'>"+object.SellExchangeName+" : "+object.SellPrice+"</td>"+
                  "<td class='align-middle text-center'>"+object.MarketName+"</td>"+
                  "<td class='align-middle text-center'>"+object.BaseCurrencyQty+"</td>"+
                  "<td class='align-middle text-center'>"+object.CurrencyQty+"</td>"+
                  "<td class='align-middle text-center'>"+object.ProfitAmt+"</td>"+
                  "</tr>";

                  valueColumn+= "<tr style='background:#808080'>" +
                          "<td class='align-middle' colspan = 8 >"+
                           "Remark : " + object.Remark+
                           "<br/>Buy Remark : " + object.BuyRemark+
                           "<br/>Sell Remark : " + object.SellRemark+
                          "</td>"+
                      "</tr>";


                });
                $(".row-data-load-daily-balances").html(valueColumn);
              },error:function(){
                  console.log(data);
              }
          });
        };
    </script>
@endsection
