<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Session;
/**
 * User : Duong Hoai Sang
 * Note : AllocatecapitalsController
 */
class AllocatecapitalsController extends Controller
{

    /**
     * User : Duong Hoai Sang.
     * Note : Show list markets.
     */
    public function showListAllocatecapitals(Request $request){
        $menu = 'allocatecapitals';
        $message = "";
        $condition = [];
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('allocatecapitals_condition', $condition);
        }
        else{
            if(Session::get('allocatecapitals_condition')){
                $condition = Session::get('allocatecapitals_condition');
            }
        }
        return view('admin.allocatecapitals.getlistallocatecapitals', compact('condition','message','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     */
    public function getListAllocatecapitals(Request $request){
        $condition = [];
        $url = 'allocatecapitals?pagesize=50';
        $page = $request->page ? $request->page : 1;
        if(Session::get('allocatecapitals_condition')){
            $condition = Session::get('allocatecapitals_condition');
        }
        if(@$condition['event_id'] != ''){
            $url = 'allocatecapitals?pagesize=50&exchangeid='.$condition['event_id'];
        }
        $url .= '&pageindex='.$page;
        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }


    public function addAllocatecapitals(Request $request){
        $message = '';
        if ($request->isMethod('post')) {
            $data = $request->data;
            $datainsert = [
                'Currency'        => $data['Currency'],
                'ExchangeId'       => $data['ExchangeId'],
                'Qty'       => $data['Qty']
            ];

            $url = 'allocatecapitals';
            $dataResponse = $this->postEndPointApi($url,$datainsert);

            Session::put('message', "Add Allocatecapitals success.");
            return redirect('/admin/allocatecapitals');
        }else{
          $data = [];
        }
        $menu = 'allocatecapitals';
        return view('admin.allocatecapitals.addallocatecapitals', compact('menu', 'data','message'));
    }

    public function editAllocatecapitals(Request $request,$id)
    {
        $message = '';
        if ($request->isMethod('post')) {
            $dataUpdate = $request->data;

            $url = 'allocatecapitals';
            $dataResponse = $this->putEndPointApi($url,$id,$dataUpdate);
            return redirect('/admin/allocatecapitals')->with('message','Edit Allocatecapitals Successfully!');
        }else{
            $url = 'allocatecapitals';
            $dataResponse = $this->getByEndPointApi($url,$id);
            $dataResponse = json_decode($dataResponse,true);
            $data = $dataResponse['Result'];
            $menu = 'allocatecapitals';
            return view('admin.allocatecapitals.editallocatecapitals', compact('menu','data'));
        }
        $menu = 'allocatecapitals';
        return view('admin.allocatecapitals.editallocatecapitals', compact('menu', 'data','message'));
    }

    public function deleteAllocatecapitals(Request $request,$id){
        $url = 'allocatecapitals';
        $dataResponse = $this->deleteEndPointApi($url,$id);
        return redirect('/admin/allocatecapitals')->with('message','Delete Allocatecapitals Successfully!');
    }


}
