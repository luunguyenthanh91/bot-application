
/*---- placeholder4----*/
$(function() {
	// We use an inline data source in the example, usually data would
	// be fetched from a server
	var data = [],
		totalPoints = 300;
	var plot = '';
	function getRandomData() {


		if (data.length > 0) data = data.slice(1);
		$.ajax({
				type: 'GET', //THIS NEEDS TO BE GET
				url: '/admin/arbitragesignals/get-list-arbitragesignals?pagesize=100',
				dataType: 'json',
				success: function (dataaa) {
					//var obj = JSON.parse(data.data);
					var obj = JSON.parse(dataaa.data);
					var arraydata = obj.Result;
					var valueColumn='';
					data = [];
					arraydata.forEach(function(object) {
						data.push(parseFloat(object.ProfitPct));
					});

				},error:function(){
						console.log(data);
				}
		});

		var res = [];
		for (var i = 0; i < data.length; ++i) {
			res.push([i, data[i]])
		}
		return res;



	}
	var updateInterval = 5000;
	$("#updateInterval").val(updateInterval).change(function() {
		var v = $(this).val();
		if (v && !isNaN(+v)) {
			updateInterval = +v;
			if (updateInterval < 1) {
				updateInterval = 1;
			} else if (updateInterval > 2000) {
				updateInterval = 2000;
			}
			$(this).val("" + updateInterval);
		}
	});


	function update() {
		plot.setData([getRandomData()]);
		plot.draw();
		setTimeout(update, updateInterval);
	}
	function getSignal(){
			$.ajax({
					type: 'GET', //THIS NEEDS TO BE GET
					url: '/admin/arbitragesignals/get-list-arbitragesignals?pagesize=100',
					dataType: 'json',
					success: function (dataaa) {
						//var obj = JSON.parse(data.data);
						var obj = JSON.parse(dataaa.data);
						var arraydata = obj.Result;
						var valueColumn='';
						arraydata.forEach(function(object) {
							data.push(parseFloat(object.ProfitPct));
						});
						plot = $.plot("#placeholder4", [getRandomData()], {
							series: {
								shadowSize: 0 // Drawing is faster without shadows
							},
							grid: {
								borderColor: "rgba(255,255,255,0.2)",
							},
							colors: ["#ff685c "],
							yaxis: {
								min: 0,
								max: 10,
								tickLength: 0
							},
							xaxis: {
								tickLength: 0,
								show: false
							}
						});

						update();
					},error:function(){
							console.log(data);
					}
			});
	}
	getSignal();
});

// A custom label formatter used by several of the plots
function labelFormatter(label, series) {
	return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}
//
function setCode(lines) {
	$("#code").text(lines.join("\n"));
}
