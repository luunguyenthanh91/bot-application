<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Session;
/**
 * User : Duong Hoai Sang
 * Note : DailyBalancesController
 */
class DailyBalancesController extends Controller
{

    /**
     * User : Duong Hoai Sang.
     * Note : Show list daily balances.
     */
    public function list(Request $request){
        $menu = 'daily-balances';
        $message = "";
        $condition = [];
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('daily_balances_condition', $condition);
        }
        else{
            if(Session::get('daily_balances_condition')){
                $condition = Session::get('daily_balances_condition');
            }
        }

        return view('admin.dailybalances.getlist', compact('condition','message','menu'));
    }
    public function total(Request $request){
        $menu = 'daily-balances';
        $dateNow = date_create()->format('Y-m-d');
        $url_dailybalances = 'dailybalances?userid=999&fromdate=2010-01-01&pagesize=2000&todate='.$dateNow;


        $dataResponse = $this->getEndPointApi($url_dailybalances);
        $dataResponse = json_decode($dataResponse);
        $data_report = [];
        foreach ($dataResponse->Result as $key => $value) {
            if(@$data_report[$value->ExchangeName]){
                if(@$data_report[$value->ExchangeName][$value->Currency]){


                }else{
                  $data_report[@$value->ExchangeName][$value->Currency]['Available'] =  $value->Available;
                  $data_report[@$value->ExchangeName][$value->Currency]['InitialBalance'] =  $value->InitialBalance;
                  $data_report[@$value->ExchangeName][$value->Currency]['Diff'] =  $value->DiffPct;
                  $data_report[@$value->ExchangeName][$value->Currency]['TotalBtc'] =  $value->TotalBtc;
                  $data_report[@$value->ExchangeName][$value->Currency]['TotalUsdt'] =  $value->TotalUsdt;
                }
            }else{

              $data_report[@$value->ExchangeName] = [];
              $data_report[@$value->ExchangeName][$value->Currency]['Available'] =  $value->Available;
              $data_report[@$value->ExchangeName][$value->Currency]['InitialBalance'] =  $value->InitialBalance;
              $data_report[@$value->ExchangeName][$value->Currency]['Diff'] =  $value->DiffPct;
              $data_report[@$value->ExchangeName][$value->Currency]['TotalBtc'] =  $value->TotalBtc;
              $data_report[@$value->ExchangeName][$value->Currency]['TotalUsdt'] =  $value->TotalUsdt;
            }
        }

        return view('admin.dailybalances.total', compact('data_report','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     * Request $request
     */
    public function getListDailyBalances(Request $request){
        $dateNow = date_create()->format('Y-m-d');
        $condition = [];
        // if($request->page){
        //     $page = $request->page;
        // }else{
        //     $page = 1;
        // }
        $url = 'dailybalances?userid=999&fromdate=2010-01-01&pagesize=999&todate='.$dateNow;
        if(Session::get('daily_balances_condition')){
            $condition = Session::get('daily_balances_condition');
        }
        if(@$condition['event_id'] != ''){
            $url .= '&exchangeid='.$condition['event_id'];
        }

        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Show list daily balances exchangesummaries.
     */
    public function listExchangesummaries(){
        $menu = 'daily-balances';
        $message = "";
        return view('admin.dailybalances.getlistexchangesummaries', compact('message','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     */
    public function getListDailyBalancesExchangesummaries(){
        $dateNow = date_create()->format('Y-m-d');
        $url = 'dailybalances/exchangesummaries?userid=999&fromdate=2010-01-01&pagesize=999&todate='.$dateNow;
        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Show list daily balances exchangesummaries.
     */
    public function listSummaries(){
        $menu = 'daily-balances';
        $message = "";
        return view('admin.dailybalances.getlistsummaries', compact('message','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     */
    public function getListSummaries(){
        $dateNow = date_create()->format('Y-m-d');
        $url = 'dailybalances/summaries?userid=999&fromdate=2010-01-01&pagesize=999&todate='.$dateNow;
        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Show list daily balances dailyprofit.
     */
    public function listDailyprofit(){
        $menu = 'daily-balances';
        $message = "";
        return view('admin.dailybalances.getlistdailyprofit', compact('message','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     */
    public function getListDailyprofit(){
        $dateNow = date_create()->format('Y-m-d');
        $url = 'dailybalances/dailyprofit?userid=999&fromdate=2010-01-01&pagesize=999&todate='.$dateNow;
        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }
    public function getListDailyprofitWithParam(Request $request){
        $dateNow = date_create()->format('Y-m-d');
        $url = 'dailybalances/dailyprofit?userid=999&fromdate=2010-01-01&pagesize=999&todate='.$dateNow;
        if($request->condition){
            $url.= '&ordertype=ASC&'.$request->condition;
        }
        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }

}
