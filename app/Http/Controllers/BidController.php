<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Bid;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BidController extends Controller
{
    public function index($ad_id){
        $user = Auth::user();
        $user_id = $user->id;
        $ad = Ad::find($ad_id);

        $title = trans('app.bids_for').' '.$ad->title;

        if (! $user->is_admin()){
            if ($ad->user_id != $user_id){
                return view('admin.error.error_404');
            }
        }
        return view('admin.bids', compact('title', 'ad'));
    }
    public function postBidAjax(Request $request , $ad_id){
        if ( ! Auth::check()){

              return $this->dataError(trans('app.login_first_to_post_bid'),[],422);
        }
        $user = Auth::user();
        $bid_amount = $request->bid_amount;

        $ad = Ad::find($ad_id);
        $current_max_bid = $ad->current_bid();

        if ($bid_amount <= $current_max_bid ){
              return $this->dataError(trans('app.enter_min_bid_amount'),[],422);
        }

        $data = [
            'ad_id'         => $ad_id,
            'user_id'       => $user->id,
            'bid_amount'    => $bid_amount,
            'is_accepted'   => 0,
        ];


        Bid::create($data);
        return $this->dataSuccess(trans('app.your_bid_posted'),[],200);
    }

    public function postBid(Request $request , $ad_id){
        if ( ! Auth::check()){
            return redirect(route('login'))->with('error', trans('app.login_first_to_post_bid'));
        }
        $user = Auth::user();
        $bid_amount = $request->bid_amount;

        $ad = Ad::find($ad_id);
        $current_max_bid = $ad->current_bid();

        if ($bid_amount <= $current_max_bid ){
            return back()->with('error', sprintf(trans('app.enter_min_bid_amount'), themeqx_price($current_max_bid)) );
        }

        $data = [
            'ad_id'         => $ad_id,
            'user_id'       => $user->id,
            'bid_amount'    => $bid_amount,
            'is_accepted'   => 0,
        ];


        Bid::create($data);
        return back()->with('success', trans('app.your_bid_posted'));
    }

    public function bidAction(Request $request){
        $action = $request->action;
        $ad_id = $request->ad_id;
        $bid_id = $request->bid_id;

        $user = Auth::user();
        $user_id = $user->id;
        $ad = Ad::find($ad_id);

        if (! $user->is_admin()){
            if ($ad->user_id != $user_id){
                return ['success' => 0];
            }
        }

        $bid = Bid::find($bid_id);
        switch ($action){
            case 'accept':
                $bid->is_accepted = 1;
                $bid->save();

                $ads = Ad::find($request->ad_id);
                $ads->expired_at = date("Y-m-d", strtotime("-1 days"));
                $ads->save();
                break;
            case 'delete':
                $bid->delete();
                break;
        }
        return ['success' => 1];
    }

    public function bidderInfo($bid_id){
        $bid = Bid::find($bid_id);
        $title = trans('app.bidder_info');

        $auth_user = Auth::user();
        $user_id = $auth_user->id;
        $ad = Ad::find($bid->ad_id);

        if (! $auth_user->is_admin()){
            if ($ad->user_id != $user_id){
                return view('admin.error.error_404');
            }
        }

        $user = User::find($bid->user_id);

        return view('admin.profile', compact('title', 'user'));
    }

    public function store(Request $request)
    {
        try{
            $auctions = null;

            $title = $request->title;
            $slug = unique_slug($this->convert_slug($title));
            $data = [
                'title'             => $request->title,
                'slug'              => $slug,
                'description'       => $request->ad_description,
                'category_id'       => $request->category_id,
                'type'              => $request->type,
                'price'             => $request->price,

                'seller_name'       => Auth::user()->name,
                'seller_email'      => Auth::user()->email,
                'seller_phone'      => Auth::user()->phone,
                'country_id'        => Auth::user()->country_id,
                'address'           => Auth::user()->address,

                'category_type'     => 'auction',
                'status'            => '0',
                'user_id'           => Auth::user()->id,
                'expired_at'        => $request->date,
                'next_min'        => $request->step,
                'buy_now'        => $request->price_max,
            ];


            $auctions = Ad::create($data);

            if (get_option('ads_moderation') == 'direct_publish'){
                $data['status'] = '1';
            }

            if ( ! $request->price_plan){
                $data['price_plan'] = 'regular';
            }


            $this->uploadAdsImage($request,$auctions->id);



            return $this->dataSuccess('Tạo đấu giá thành công',$auctions,200);

        }
        catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),[],200);
        }

    }


}
