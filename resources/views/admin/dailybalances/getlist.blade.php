@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container-fluid">

    <div class="page-header">
      <h4 class="page-title">Daily Balances</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Daily Balances</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>
          <div class="row row-cards">
            <div class="col-lg-12">
              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="card">
                    <div class="card-header">
                      <div class="card-title"> Điều Kiện Lọc</div>
                    </div>
                    <div class="card-body">
                      <div class="form-group">
                        <label class="form-label">Exchanges</label>
                        <select id="event_id" name="condition[event_id]" class="form-control custom-select">

                        </select>
                        <button type="button" class="btn btn-primary btn_search col-md-12 col-lg-12  mt-3">
                          <i class="fa fa-search " aria-hidden="true"></i>
                        </button>
                      </div>



                      <!-- <div class="form-group">
                          <label class="form-label">Thêm Đồng Coin</label>
                          <a href="/admin/allocatecapitals/addallocatecapitals" class="btn btn-primary  col-md-12 col-lg-12">
                            <i class="fa fa-calendar-plus-o " aria-hidden="true"></i>
                          </a>
                      </div> -->


                      <form method="post" action="" id="form_search">
                          @csrf
                          <input type="hidden" name="condition[event_id]" id="event_id_val">
                      </form>


                    </div>
                  </div>

                </div>
              </div>
            </div>
							<div class="col-lg-12">
								<div class="card mt-5 store">
                  @if($message != '')
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$message}}
                  </div>
                  @endif
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table id="dailybalances" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                          <tr>
                            <th class="text-center">Id</th>
                            <th class="text-center">Exchange</th>
                            <th class="text-center">Currency</th>
                            <th class="text-center">TotalBtc</th>
                            <th class="text-center">TotalUsdt</th>
                            <th class="text-center">Total</th>
                            <th class="text-center">BtcBasePriceText</th>
                            <th class="text-center">Reserved</th>
                            <th class="text-center">Available</th>
                            <th class="text-center">UsdtBtcPrice</th>
                            <th class="text-center">UsdtBasePrice</th>
                            <th class="text-center">BtcBasePrice</th>
                          </tr>
                        </thead>
                        <tbody class="row-data-load-daily-balances">
                        <!-- Load data tai day -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->

  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <script type="text/javascript">
        $( document ).ready(function() {
          // setInterval(function() {
          // }, 5000);

          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_list_daily_balances")}}',
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='';
                arraydata.forEach(function(object) {
                  //console.log(object.ExchangeName);
                  valueColumn+= "<tr><td class='align-middle text-center'>"+object.Id+"</td>"+
                  "<td class='align-middle text-center'>"+object.ExchangeName+"</td>"+
                  "<td class='align-middle text-center'>"+object.Currency+"</td>"+
                  "<td class='align-middle text-center'>"+object.BtcBasePriceText+"</td>"+
                  "<td class='align-middle text-center'>"+object.TotalBtc+"</td>"+
                  "<td class='align-middle text-center'>"+object.TotalUsdt+"</td>"+
                  "<td class='align-middle text-center'>"+object.Total+"</td>"+
                  "<td class='align-middle text-center'>"+object.Reserved+"</td>"+
                  "<td class='align-middle text-center'>"+object.Available+"</td>"+
                  "<td class='align-middle text-center'>"+object.UsdtBtcPrice+"</td>"+
                  "<td class='align-middle text-center'>"+object.UsdtBasePrice+"</td>"+
                  "<td class='align-middle text-center'>"+object.BtcBasePrice+"</td>"+
                  "</tr>"
                });
                $(".row-data-load-daily-balances").html(valueColumn);

                // $('#dailybalances').DataTable({
                //   data:  arraydata,
                //   "pageLength": 25,
                //   columns: [
                //     { data: 'Id' },
                //     { data: 'ExchangeName' },
                //     { data: 'Currency' },
                //     { data: 'BaseCurrency' },
                //     { data: 'BtcBasePriceText' },
                //     { data: 'TotalBtc' },
                //     { data: 'TotalUsdt' },
                //     { data: 'Total' },
                //     { data: 'Reserved' },
                //     { data: 'Available' },
                //     { data: 'UsdtBtcPrice' },
                //     { data: 'UsdtBasePrice' },
                //     { data: 'BtcBasePrice' }
                //   ]
                // });

              },error:function(){
                  console.log(data);
              }
          });

          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_list_exchanges")}}',
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='<option value="">--Tất Cả--</option>';

                arraydata.forEach(function(object) {
                  //Check URL website.
                  if(object.WebUrl=='' || object.WebUrl == undefined){
                    webUrl = '...';
                  }else{
                    webUrl = object.WebUrl;
                  }
                  if(('{{@$condition['event_id']}}') == object.Id){
                      valueColumn+= '<option selected value="'+object.Id+'">'+object.Name+'</option>';
                  }else{
                    valueColumn+= '<option value="'+object.Id+'">'+object.Name+'</option>';
                  }

                });
                $("#event_id").html(valueColumn);
              },error:function(){
                  console.log(data);
              }
          });

          $('.btn_search').on("click",function(){
              var event_id = $("#event_id").val();
              $("#event_id_val").val(event_id);
              $("#form_search").submit();
          });

        });
    </script>
@endsection
