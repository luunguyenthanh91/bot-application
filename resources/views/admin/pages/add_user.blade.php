@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container">

    <div class="page-header">
      <h4 class="page-title">Thành Viên</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item"><a href="/admin/users">Quản Lý Thành Viên</a></li>
        <li class="breadcrumb-item active" aria-current="page">Thêm Thành Viên</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
          <form action="" method="post">
            @csrf
            <div class="card">
              <div class="card-header">
                <h3 class="mb-0 card-title">Thông tin thành viên</h3>
              </div>
              <div class="card-body">

                @if(@$message != '')
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <p>- {{$message}}</p>
                </div>
                @endif

                @if(@$errors->all())
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  @foreach ($errors->all() as $item)
                  <p>- {{$item}}</p>
                  @endforeach
                </div>
                @endif

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" required class="form-control" name="data[name]" placeholder="Tên người dùng" value="{{@$data['name']}}">
                    </div>
                    <div class="form-group">
                      <input type="email" required class="form-control" name="data[email]" placeholder="Email" value="{{@$data['email']}}" >
                    </div>
                    <div class="form-group">
                      <input type="text" required class="form-control" name="data[phone]" placeholder="Số điện thoại" value="{{@$data['phone']}}" >
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group has-success">
                      <input type="text" required class="form-control" name="data[address]" placeholder="Địa chỉ" value="{{@$data['address']}}">
                    </div>
                    <div class="form-group  has-danger">
                      <input type="password" required class="form-control" name="data[password]" placeholder="Mật Khẩu" value="{{@$data['password']}}">
                    </div>
                    <div class="form-group">
                      <input type="password" required class="form-control" name="data[re_password]" placeholder="Nhập lại mật khẩu" value="{{@$data['re_password']}}">
                    </div>
                  </div>

                </div>
              </div>
              <div class="card-footer text-right">
    						<button type="submit" class="btn btn-primary">Thêm Thành Viên</button>
    					</div>

            </div>
        </form>
      </div>

    </div>

  </div>
  <!--footer-->


  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <!-- <script src="{{ asset('admins/assets/js/index3.js') }}"></script> -->
    <!-- <script src="{{ asset('admins/assets/js/index3-worldmap.js') }}"></script> -->
    <link href="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet" />
    <script src="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('admins/assets/js/sweet-alert.js') }}"></script>
    <script>

        $( document ).ready(function() {
            $('.btn_search').on("click",function(){
                var email = $("#email").val();
                var phone = $("#phone").val();
                var type_login = $("#type_login").val();
                $("#type_login_val").val(type_login);
                $("#phone_val").val(phone);
                $("#email_val").val(email);
                $("#form_search").submit();
            });
            $(".delete_conform").on("click",function(){
                var id = $(this).attr('id');
                swal({
            			title: "Xác nhận",
            			text: "Bạn đồng ý xoá thành viên này ?",
            			type: "warning",
            			showCancelButton: true,
            			confirmButtonText: 'Có',
            			cancelButtonText: 'Không'
            		}, function(inputValue) {
                    if(inputValue){
                      var url= "/admin/delete-user/"+id;
                      window.location = url;
                    }

            		});
            });
        });
    </script>
@endsection
