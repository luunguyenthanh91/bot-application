@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container-fluid">

    <div class="page-header">
      <h4 class="page-title">Daily Balances Exchangesummaries</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Daily Balances Exchangesummaries</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>
          <div class="row row-cards">
							<div class="col-lg-12">
								<div class="card mt-5 store">
                  @if($message != '')
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$message}}
                  </div>
                  @endif
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table id="dailybalancesexchangesummaries" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                          <tr>
                          <th class="text-center">Id</th>
                            <th class="text-center">ExchangeName</th>
                            <th class="text-center">TotalBtc</th>
                            <th class="text-center">TotalUsdt</th>
                          </tr>
                        </thead>
                        <tbody>
                        <!-- Load data tai day -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->

  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <script type="text/javascript">
        $( document ).ready(function() {
          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_daily_balances_exchangesummaries")}}',
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);  
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                // var valueColumn='';
                // arraydata.forEach(function(object) {
                //   //console.log(object.ExchangeName);
                //   valueColumn+= "<tr><td class='align-middle text-center'>"+object.ExchangeName+"</td>"+
                //   "<td class='align-middle text-center'>"+object.TotalBtc+"</td>"+
                //   "<td class='align-middle text-center'>"+object.TotalUsdt+"</td>"+
                //   "</tr>"
                // });
                // $(".row-data-load-exchangesummaries").html(valueColumn);

                $('#dailybalancesexchangesummaries').DataTable({
                  data:  arraydata,
                  "pageLength": 25,
                  columns: [
                    { data: 'Id' },
                    { data: 'ExchangeName' },
                    { data: 'TotalBtc' },
                    { data: 'TotalUsdt' }
                  ]
                });

              },error:function(){ 
                  console.log(data);
              }
          });
        });
    </script>
@endsection
