<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use Session;

/**
 * User : Duong Hoai Sang
 * Note : DepositaddressController
 */
class DepositaddressController extends Controller
{

    /**
     * User : Duong Hoai Sang.
     * Note : Show list markets.
     */
    public function showListDepositaddress(Request $request){
        $menu = 'depositaddress';
        $message = "";
        $condition = [];
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('depositaddress_condition', $condition);
        }
        else{
            if(Session::get('depositaddress_condition')){
                $condition = Session::get('depositaddress_condition');
            }
        }
        return view('admin.depositaddress.getlistdepositaddress', compact('condition','message','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     */
    public function getListDepositaddress(Request $request){

      $condition = [];
      $url = 'depositaddress?pagesize=30';
      $page = $request->page ? $request->page : 1;
      if(Session::get('depositaddress_condition')){
          $condition = Session::get('depositaddress_condition');
      }
      if(@$condition['event_id'] != ''){
          $url = 'depositaddress?pagesize=30&exchangeid='.$condition['event_id'];
      }
      $url .= '&pageindex='.$page;
      $dataResponse = $this->getEndPointApi($url);
      return response()->json(array('success' => true, 'data' => $dataResponse));


    }

    public function addDepositaddress(Request $request){
        $message = '';
        if ($request->isMethod('post')) {
            $data = $request->data;


            $datainsert = [
                'Address'        => $data['Address'],
                'AddressTag'       => $data['AddressTag'],
                'Currency'      => $data['Currency'],
                'ExchangeId'       => $data['ExchangeId'],
                'UserId'       => 999
            ];

            $url = 'depositaddress';
            $dataResponse = $this->postEndPointApi($url,$datainsert);

            Session::put('message', "Add depositaddress success.");
            return redirect('/admin/depositaddress');
        }else{
          $data = [];
        }
        $menu = 'depositaddress';
        return view('admin.depositaddress.adddepositaddress', compact('menu','data','message'));
    }

    public function editDepositaddress(Request $request,$id)
    {
        $message = '';
        if ($request->isMethod('post')) {
            $dataUpdate = $request->data;


            $url = 'depositaddress';
            $dataResponse = $this->putEndPointApi($url,$id,$dataUpdate);
            return redirect('/admin/depositaddress')->with('message','Edit Depositaddres successfully!');
        }else{
            $url = 'depositaddress';
            $dataResponse = $this->getByEndPointApi($url,$id);
            $dataResponse = json_decode($dataResponse,true);
            $data = $dataResponse['Result'];
            return view('admin.depositaddress.editdepositaddress', compact('data'));
        }
        $menu = 'depositaddress';
        return view('admin.depositaddress.editdepositaddress', compact('menu', 'data','message'));
    }

    public function deleteDepositaddress(Request $request,$id){
        $url = 'depositaddress';
        $dataResponse = $this->deleteEndPointApi($url,$id);
        return redirect('/admin/depositaddress')->with('message','Delete Depositaddres Ssuccessfully!');
    }

}
