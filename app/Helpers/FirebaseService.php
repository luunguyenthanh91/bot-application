<?php

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseService
{

    public static function fcm($deviceToken, $title, $body,$data,$type)
    {
        $data['type'] = $type;
        $serviceAccount = ServiceAccount::fromJsonFile(public_path('firebase/firebase.json'));
        $firebase = (new Factory())->withServiceAccount($serviceAccount)->create();

        return $firebase
            ->getMessaging()
            ->send([
                'token' => $deviceToken,
                // 'condition' => "'TopicA' in topics && ('TopicB' in topics || 'TopicC' in topics)",
                // 'token' => '...',
//                'notification' => [
//                    'title' => $title,
//                    'body' => $body,
//                    'color' => '#f45342',
//                    'sound' => 'default'
//
//                ],
                'data' => $data,
                'android' => [
                    'ttl' => '3600s',
                    'priority' => 'high',
                    'data' => [
                        'title' => $title,
                        'body' => $body,
                        'icon' => 'stock_ticker_update',
                        'color' => '#f45342',
                    ],
                ],
                'apns' => [
                    'headers' => [
                        'apns-priority' => '10',
                    ],
                    'payload' => [
                        'aps' => [
                            'alert' => [
                                'title' => $title,
                                'body' => $body,
                            ],
                            'badge' => 0,
                            'sound' => "default"
                        ],
                    ],
                ],
                'webpush' => [
                    'notification' => [
                        'title' => $title,
                        'body' => $body,
                        'icon' => 'https://my-server/icon.png',
                    ],
                ],
            ]);

//        $message = MessageToRegistrationToken::create($deviceToken)
//            ->withNotification($notification) // optional
//            ->withApnsConfig($ios)
//            ->withAndroidConfig($android);
//        try {
//            $firebase->getMessaging()->validate($message);
//        } catch (InvalidMessage $e) {
//            return ($e->errors());
//        }
//        return $firebase->getMessaging()->send($message);
    }

}

