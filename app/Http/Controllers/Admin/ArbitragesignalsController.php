<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

/**
 * User : Duong Hoai Sang
 * Note : ArbitragesignalsController
 */
class ArbitragesignalsController extends Controller
{

    /**
     * User : Duong Hoai Sang.
     * Note : Show list Arbitragesignals.
     */
    public function showListArbitragesignals(){
        $menu = 'arbitragesignals';
        $message = "";
        return view('admin.arbitragesignals.showlist', compact('message','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     */
    public function getListArbitragesignals(Request $request){
        $url = 'arbitragesignals';
        if($request->page){
            $page = $request->page;
            $url .= '?pageindex='.$page;
        }else{
            $page = 1;
            $url .= '?pageindex='.$page;
        }
        if($request->pagesize){
            $pagesize = $request->pagesize;
            $url .= '&pagesize='.$pagesize;
        }else{
            $pagesize = 30;
            $url .= '&pagesize='.$pagesize;
        }
        //arbitragesignals?buyexchangeid=3&sellexchangeid=9&basecurrency=USDT&currency=ZIL&pagesize=30&pageindex=1

        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }

}
