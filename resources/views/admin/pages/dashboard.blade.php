@extends('layouts.admin_main')

@section('content')
<ng-controller ng-controller="AppController as demo">
    <div class="content-area">
                <div class="container">


                    <div class="page-header">
                        <h4 class="page-title">Dashboard</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Bigdata Dashboard</li>
                        </ol>
                    </div>
                    <div class="row row-cards">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="card crypt-marketing-values">
                                <div class="card-header">
                                    <div class="card-title">Crypt Marketing Values</div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table border table-bordered mb-0 text-nowrap">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Icon</th>
                                                    <th>Currency</th>
                                                    <th>Price</th>
                                                    <th>Market Cap</th>
                                                    <th>Volume 1D</th>
                                                    <th>Change % (1D)</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-gray">
                                                <tr>
                                                    <td>1</td>
                                                    <td><img src="{{ asset('admins/assets/images/crypto-currencies/bitcoin.svg')}}" class="w-5 h-5" alt=""></td>
                                                    <td>Bitcoin</td>
                                                    <td>$ {{$btc[0]->close}}</td>
                                                    <td>$ {{$btc[0]->market_cap}}</td>
                                                    <td>$ {{$btc[0]->volume}}</td>
                                                    <td><span class="badge badge-gradient-primary"> {{$btc[0]->pacent}}%</span></td>

                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td><img src="{{ asset('admins/assets/images/crypto-currencies/ethereum.svg')}}" class="w-5 h-5" alt=""></td>
                                                    <td>Ethereum</td>
                                                    <td>$ {{$eth[0]->close}}</td>
                                                    <td>$ {{$eth[0]->market_cap}}</td>
                                                    <td>$ {{$eth[0]->volume}}</td>
                                                    <td><span class="badge badge-gradient-primary"> {{$eth[0]->pacent}}%</span></td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td><img src="{{ asset('admins/assets/USDT.png')}}" class="w-5 h-5" alt=""></td>
                                                    <td>USDT</td>
                                                    <td>$ {{$usdt[0]->close}}</td>
                                                    <td>$ {{$usdt[0]->market_cap}}</td>
                                                    <td>$ {{$usdt[0]->volume}}</td>
                                                    <td><span class="badge badge-gradient-primary"> {{$usdt[0]->pacent}}%</span></td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 ">
                            <div class="card overflow-hidden">
                                <div class="card-header">
                                    <h3 class="card-title">Bitcoin</h3>
                                    <div class="card-options">
                                        <img src="{{ asset('admins/assets/images/crypto-currencies/bitcoin.svg')}}" class="w-6 h-6" alt="">
                                    </div>
                                </div>
                                <div class="card-body pb-0">
                                    <h3 class="mb-3" id="count_btc"></h3>
                                </div>
                                <div class="chart-wrapper ">
                                    <canvas id="AreaChart1" class="mb-0 p-0 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <div class="card overflow-hidden">
                                <div class="card-header">
                                    <h3 class="card-title">Ethereum</h3>
                                    <div class="card-options">
                                        <img src="{{ asset('admins/assets/images/crypto-currencies/ethereum.svg')}}" class="w-6 h-6" alt="">
                                    </div>
                                </div>
                                <div class="card-body pb-0">
                                    <h3 class="mb-3" id="count_eth"></h3>
                                </div>
                                <div class="chart-wrapper ">
                                    <canvas id="AreaChart2" class="mb-0 p-0 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <div class="card overflow-hidden">
                                <div class="card-header">
                                    <h3 class="card-title">USDT</h3>
                                    <div class="card-options">
                                        <img src="{{ asset('admins/assets/USDT.png')}}" class="w-6 h-6" alt="">
                                    </div>
                                </div>
                                <div class="card-body pb-0">
                                    <h3 class="mb-3" id="count_usdt"></h3>
                                </div>
                                <div class="chart-wrapper ">
                                    <canvas id="AreaChart3" class="mb-0 p-0 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
                            <div class="card overflow-hidden">
                                <div class="card-header">
                                    <h3 class="card-title">Signal</h3>
                                    <div class="card-options">
                                        <img src="{{ asset('admins/assets/images/crypto-currencies/bitcoin.svg')}}" class="w-6 h-6" alt="">
                                    </div>
                                </div>
                                <div class="chart-wrapper " >
                                    <canvas id="AreaChart4" class="mb-0 p-0 chart-dropshadow"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 ">
                            <div class="card overflow-hidden">
                                <div class="card-header">
                                    <h3 class="card-title">Signal Detail</h3>
                                    <div class="card-options">
                                        <img src="{{ asset('admins/assets/images/crypto-currencies/bitcoin.svg')}}" class="w-6 h-6" alt="">
                                    </div>
                                </div>
                                <div class="chart-wrapper "style="height: 300px;overflow: auto;">
                                  <div class="e-table">
                                    <div class="table-responsive table-lg">
                                      <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th class="text-center">PAIRING</th>
                                            <th class="text-center">TIME (ms)</th>
                                            <th class="text-center">SELL</th>
                                            <th class="text-center">BUY</th>
                                            <th class="text-center">% PROFIT</th>
                                          </tr>
                                        </thead>
                                        <tbody id="myList" class="row-data-load-daily-balances">
                                          <tr ng-repeat="item in allsignal" ng-class="item.status == 1 ? 'red-tabel-row' : ''">
                                            <td class='align-middle text-center'>((item.MarketName))</td>
                                            <td class='align-middle text-center'>((item.CheckingDuration))</td>
                                            <td class='align-middle text-center'>((item.SellExchangeName)) / ((item.SellPrice)) / ((item.SellAmount))</td>
                                            <td class='align-middle text-center'>((item.BuyExchangeName)) / ((item.BuyPrice)) / ((item.BuyAmount))</td>
                                            <td class='align-middle text-center'>((item.ProfitPct))</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>

                    </div>

        						<div style="display:none" class="row row-deck">
        							<div class="col-xl-6 col-sm-6 col-md-6 col-lg-6">
        							    <div class="card">
        									<div class="card-header">
        									    <h3 class="card-title">Data Analysis</h3>
        									</div>
        								    <div class="card-body">
        										<div id="timeline-chart" class="overflow-hidden chart-dropshadow"></div>
        									</div>
        								</div>
        							</div>
        						</div>




                </div>
                <!--footer-->
                <footer class="footer">
                    <div class="container">
                        <div class="row align-items-center flex-row-reverse">
                            <div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
                                Copyright © 2019 <a href="#">PrimeArbitron BOT</a>. Designed by <a href="#">PrimeArbitron</a> All rights reserved.
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer-->
			</div>
</ng-controller>

@endsection


@section('page-js')

<script src="{{ asset('admins/assets/js/index5.js')}}"></script>
<script>
  customInterpolationApp.controller('AppController', function($scope, $http) {
      $scope.allsignal = [];
      function loadRealtimeArbitragesignals(){
          $http.get("{{route("get_list_arbitragesignals")}}")
          .then(function(response) {
              var obj = JSON.parse(response.data.data);
              var arraydata = obj.Result;
              angular.forEach($scope.allsignal, function(value, key) {
                  value.status = 0;
              });
              arraydata.reverse();
              var date = [];
              var value_date = [];
              angular.forEach(arraydata, function(value, key) {
                  if (!$scope.allsignal.some((item) => item.Id == value.Id)) {
                      value.status = 1;
                      $scope.allsignal.unshift(value);
                  }

                  date.push(value.Date);
                  value_date.push(value.ProfitPct);
                  var ctx = document.getElementById( "AreaChart4" );
                  ctx.height = 300;
                  var myChart = new Chart( ctx, {
                      type: 'line',
                      data: {
                          labels: date,
                          type: 'line',
                          datasets: [ {
                              data: value_date,
                              label: '% PROFIT',
                              backgroundColor: 'rgba(255, 104, 92, 0.1)',
                              borderColor: 'rgba(255, 104, 92, 0.9)',
                          }, ]
                      },
                      options: {

                          maintainAspectRatio: false,
                          legend: {
                              display: false
                          },
                          responsive: true,
                          tooltips: {
                              mode: 'index',
                              titleFontSize: 12,
                              titleFontColor: '#888080',
                              bodyFontColor: '#888080',
                              backgroundColor: '#fff',
                              titleFontFamily: 'Montserrat',
                              bodyFontFamily: 'Montserrat',
                              cornerRadius: 3,
                              intersect: false,
                          },
                          scales: {
                              xAxes: [ {
                                  gridLines: {
                                      color: 'transparent',
                                      zeroLineColor: 'transparent'
                                  },
                                  ticks: {
                                      fontSize: 2,
                                      fontColor: 'transparent'
                                  }
                              } ],
                              yAxes: [ {
                                  display:false,
                                  ticks: {
                                      display: false,
                                  }
                              } ]
                          },
                          title: {
                              display: false,
                          },
                          elements: {
                              line: {
                                  borderWidth: 1
                              },
                              point: {
                                  radius: 4,
                                  hitRadius: 10,
                                  hoverRadius: 4
                              }
                          }
                      }
                  } );

              });
          });
      }
      // start get data


      loadRealtimeArbitragesignals();
      setInterval(function() {
        loadRealtimeArbitragesignals();
      }, 5000);


  });
</script>



@endsection
