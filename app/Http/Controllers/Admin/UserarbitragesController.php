<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Session;
/**
 * User : Duong Hoai Sang
 * Note : UserarbitragesController
 */
class UserarbitragesController extends Controller
{

    /**
     * User : Duong Hoai Sang.
     * Note : Show list markets.
     */
    public function showListUserarbitrages(Request $request){
        $menu = 'userarbitrages';
        $message = "";
        $message = Session::get('message', "Add userarbitrages success.");
        $condition = [];
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('userarbitrages_condition', $condition);
        }
        else{
            if(Session::get('userarbitrages_condition')){
                $condition = Session::get('userarbitrages_condition');
            }
        }
        return view('admin.userarbitrages.getlistuserarbitrages', compact('condition','message','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     */
    public function getListUserarbitrages(Request $request){
        $condition = [];
        $url = 'userarbitrages?pagesize=50';
        $page = $request->page ? $request->page : 1;
        if(Session::get('userarbitrages_condition')){
            $condition = Session::get('userarbitrages_condition');
        }
        if(@$condition['event_id'] != ''){
            $url = 'userarbitrages?pagesize=50&basecurrency='.$condition['event_id'];
        }
        $url .= '&pageindex='.$page;
        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }

    public function start(Request $request , $id){
            $url = 'userarbitrages/'.$id.'/start';
            $dataResponse = $this->postEndPointApi($url,[]);
            response()->json(array('success' => true, 'data' => []));
    }

    public function stop(Request $request , $id){
            $url = 'userarbitrages/'.$id.'/stop';
            $dataResponse = $this->postEndPointApi($url,[]);
            response()->json(array('success' => true, 'data' => []));
    }

    public function add(Request $request){
        $message = '';
        if ($request->isMethod('post')) {
            $data = $request->data;
            if(!@$data['IsExecuteSignal']){
                $data['IsExecuteSignal'] = false;
            }

            $code_verify = Session::get('code_verify');
            if($request->code == $code_verify){
              $datainsert = [
                  'IsExecuteSignal'        => $data['IsExecuteSignal'],
                  'MarketName'       => $data['MarketName'],
                  'MinProfitPct'      => $data['MinProfitPct'],
                  'Note'       => $data['Note'],
                  'UserId'       => 999,
                  'IsActive' => 'true'
              ];

              $url = 'userarbitrages';
              $dataResponse = $this->postEndPointApi($url,$datainsert);
              // echo '<pre>';
              // print_r($dataResponse);die;
              Session::put('message', "Add userarbitrages success.");
              return redirect('/admin/userarbitrages');
            }else{
              Session::put('message', "Code confirm not working.");
              return redirect('/admin/userarbitrages');
            }


        }else{
          $data = [];
        }
        $menu = 'userarbitrages';
        return view('admin.userarbitrages.add', compact('menu','data','message'));
    }

    public function edit(Request $request,$id)
    {
        $message = '';
        if ($request->isMethod('post')) {
            $dataUpdate = $request->data;

            $code_verify = Session::get('code_verify');
            if($request->code == $code_verify){
                $url = 'userarbitrages';
                $dataResponse = $this->putEndPointApi($url,$id,$dataUpdate);
                return redirect('/admin/userarbitrages')->with('message','Edit Userarbitrages successfully!');
            }else{
                return redirect('/admin/userarbitrages')->with('message','Code confirm not working!');
            }

        }else{
            $url = 'userarbitrages';
            $dataResponse = $this->getByEndPointApi($url,$id);
            $dataResponse = json_decode($dataResponse,true);
            $data = $dataResponse['Result'];
            $menu = 'userarbitrages';
            return view('admin.userarbitrages.edit', compact('menu','data'));
        }
        $menu = 'userarbitrages';
        return view('admin.userarbitrages.edit', compact('menu', 'data','message'));
    }

    public function updateIsExecuteSignal(Request $request){
        $input = $request->all();
        $data = array();
        $data['MarketName'] = $input['marketname'];
        $data['IsExecuteSignal'] = $input['IsExecuteSignal'];
        $url = 'userarbitrages';
        $dataResponse = $this->putEndPointApi($url,$input['id'], $data);
        return response()->json(['success'=>"Edit Userarbitrages successfully!"]);
    }



    public function delete(Request $request,$id){
        $url = 'userarbitrages';
        $dataResponse = $this->deleteEndPointApi($url,$id);
        return redirect('/admin/userarbitrages')->with('message','Delete Userarbitrages Successfully!');
    }
}
