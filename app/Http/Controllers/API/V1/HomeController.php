<?php

namespace App\Http\Controllers\API\V1;

use App\Ad;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        try{

            $data['regular_ads'] = Ad::activeRegular()->with('category', 'city','state', 'country', 'sub_category')->limit(20)->orderBy('id', 'desc')->get();
            $data['premium_ads'] = Ad::activePremium()->with('category', 'city','state', 'country', 'sub_category')->limit(20)->orderBy('id', 'desc')->get();

            return $this->dataSuccess('Lấy danh sách đấu giá thành công',$data,200);



        }catch (\Exception $exception)
        {
            return $this->dataError($exception->getMessage(),null,200);
        }


    }
}
