<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

/**
 * User : Duong Hoai Sang
 * Note : ArbitragetransactionsController
 */
class ArbitragetransactionsController extends Controller
{
    /**
     * User : Duong Hoai Sang.
     * Note : Show list markets.
     * Request $request, $page = 1
     */
    public function showListArbitragetransactions(){
        $menu = 'arbitragetransactions';
        $message = "";
        return view('admin.arbitragetransactions.getlistarbitragetransactions',compact('message','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     */
    public function getListArbitragetransactions(){
        $url = 'arbitragetransactions?userid=999&signalid=0&IsShowFailed=true&pagesize=99';
        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }

}
