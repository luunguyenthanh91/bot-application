@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container-fluid">

    <div class="page-header">
      <h4 class="page-title">Daily Balances Dailyprofit</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Daily Balances Dailyprofit</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>
          <div class="row row-cards">
							<div class="col-lg-12">
								<div class="card mt-5 store">
                  @if($message != '')
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$message}}
                  </div>
                  @endif
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table id="dailybalancesdailyprofit" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                          <tr>
                            <th class="text-center">Date</th>
                            <th class="text-center">ProfitAmt</th>
                            <th class="text-center">BaseCurrency</th>
                          </tr>
                        </thead>
                        <tbody >
                        <!-- Load data tai day -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->

  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <script type="text/javascript">
        $( document ).ready(function() {
          // setInterval(function() {
          // }, 5000);

          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_daily_balances_dailyprofit")}}',
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                // var valueColumn='';
                // arraydata.forEach(function(object) {
                //   //console.log(object.ExchangeName);
                //   valueColumn+= "<tr><td class='align-middle text-center'></td>"+
                //   "<td class='align-middle text-center'>"+object.Date+"</td>"+
                //   "<td class='align-middle text-center'>"+object.ProfitAmt+"</td>"+
                //   "</tr>"
                // });
                // $(".row-data-load-exchangesummaries").html(valueColumn);
                $('#dailybalancesdailyprofit').DataTable({
                  data:  arraydata,
                  "pageLength": 25,
                  columns: [
                    { data: 'Date' },
                    { data: 'ProfitAmt' },
                    { data: 'BaseCurrency' }
                  ]
                });
              },error:function(){
                  console.log(data);
              }
          });
        });
    </script>
@endsection
