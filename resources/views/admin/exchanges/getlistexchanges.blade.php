@extends('layouts.admin_main')

@section('content')
<div class="content-area">
  <div class="container-fluid">

    <div class="page-header">
      <h4 class="page-title">Exchanges</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Exchanges</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>
          <div class="row row-cards">
							<div class="col-lg-12">
								<div class="card mt-5 store">
                  @if($message != '')
                  <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{$message}}
                  </div>
                  @endif
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th class="text-center">Name</th>
                            <th class="text-center">WebUrl</th>
                            <th class="text-center">TradeFeePct</th>
                            <th class="text-center">IsSupportTelegramTrade</th>
                            <th class="text-center">IsSupportWebTrade</th>
                            <th class="text-center">IsSupportArbitrage</th>
                          </tr>
                        </thead>
                        <tbody id="myList" class="row-data-load-daily-balances">
                        <!-- Load data tai day -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->

  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')
    <script type="text/javascript">
        $( document ).ready(function() {
          // setInterval(function() {
          // }, 5000);

          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_list_exchanges")}}',
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='';

                arraydata.forEach(function(object) {
                  //Check URL website.
                  if(object.WebUrl=='' || object.WebUrl == undefined){
                    webUrl = '...';
                  }else{
                    webUrl = object.WebUrl;
                  }
                  valueColumn+= "<tr><td class='align-middle text-center'>"+object.Name+"</td>"+
                  "<td class='align-middle text-center'>"+webUrl+"</td>"+
                  "<td class='align-middle text-center'>"+object.TradeFeePct+"</td>"+
                  "<td class='align-middle text-center'>"+object.IsSupportTelegramTrade+"</td>"+
                  "<td class='align-middle text-center'>"+object.IsSupportWebTrade+"</td>"+
                  "<td class='align-middle text-center'>"+object.IsSupportArbitrage+"</td>"+
                  "</tr>"
                });
                $(".row-data-load-daily-balances").html(valueColumn);
              },error:function(){
                  console.log(data);
              }
          });
        });
    </script>
@endsection
