<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Session;

/**
 * User : Duong Hoai Sang
 * Note : MarketsController
 */
class MarketsController extends Controller
{

    /**
     * User : Duong Hoai Sang.
     * Note : Show list markets.
     */
    public function showListMarkets(Request $request){
        $menu = 'markets';
        $message = "";
        $condition = [];
        if ($request->isMethod('post')) {
            $condition = @$request->condition;
            Session::put('markets_condition', $condition);
        }
        else{
            if(Session::get('markets_condition')){
                $condition = Session::get('markets_condition');
            }
        }
        return view('admin.markets.getlistmarkets', compact('condition','message','menu'));
    }

    /**
     * User : Duong Hoai Sang.
     * Note : Response json data.
     */

     public function getListMarketsAll(Request $request){
         $url = 'markets';
         if(Session::get('markets_condition')){
             $condition = Session::get('markets_condition');
         }
         if(@$condition['event_id'] != ''){
             $url = 'markets?exchangeid='.$condition['event_id'];
         }
         $dataResponse = $this->getEndPointApi($url);
         return response()->json(array('success' => true, 'data' => $dataResponse));
     }
    public function getListMarkets(Request $request){
        $url = 'markets';
        $dataResponse = $this->getEndPointApi($url);
        return response()->json(array('success' => true, 'data' => $dataResponse));
    }

}
