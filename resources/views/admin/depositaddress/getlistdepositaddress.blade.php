@extends('layouts.admin_main')

@section('content')
<style type="text/css">
  hr.tab{margin:2px;}
</style>
<div class="content-area">
  <div class="container-fluid">

    <div class="page-header">
      <h4 class="page-title">Depositaddress</h4>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/admin">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Depositaddress</li>
      </ol>
    </div>
    <div class="row row-cards">
      <div class="col-lg-12">
        <div>
          <div class="row row-cards">

              <div class="col-lg-3">
                <div class="row">

                  <div class="col-md-12 col-lg-12">
                    <div class="card">

                      <div class="card-header">
                        <div class="card-title"> Search</div>
                      </div>

                      <div class="card-body">

                        <div class="form-group col-xl-12 col-md-12 float-left">
                          <label class="form-label">EXNAME</label>
                          <select id="event_id" name="condition[event_id]" class="form-control custom-select col-xl-12 col-md-12">

                            </select>
                          <button type="button" class="btn btn-primary btn_search col-xl-12 col-md-12 col-lg-12">
                            <i class="fa fa-search " aria-hidden="true"></i>
                          </button>
                        </div>

                        <div class="form-group col-xl-12 col-md-12 float-left">
                            <label class="form-label">New Data</label>
                            <a href="/admin/depositaddress/adddepositaddress" class="btn btn-primary col-xl-12 col-md-12 col-lg-12">
                              <i class="fa fa-calendar-plus-o " aria-hidden="true"></i>
                            </a>
                        </div>


                        <form method="post" action="" id="form_search">
                            @csrf
                            <input type="hidden" name="condition[event_id]" id="event_id_val">
                        </form>


                      </div>

                    </div>

                  </div>
                </div>
              </div>


							<div class="col-lg-9">
								<div class="card store">
                  @if(session('message'))
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <strong>{{ session('message') }}</strong>
                      </div>
                  @endif
                  <div class="e-table">
                    <div class="table-responsive table-lg">
                      <table id="depositaddress" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                          <tr>
                          <th class="text-center">No.</th>
                            <th class="text-center">ExName</th>
                            <th class="text-center">Currency</th>
                            <th class="text-center" style="min-width : 110px">Action</th>
                            <th class="">Address</th>
                          </tr>
                        </thead>
                        <tbody class="row-data-load-daily-balances">
                        <!-- Load data tai day -->
                        </tbody>
                      </table>

                      <div class="card-body">
    										<div class="pagination-wrapper">
    											<nav aria-label="Page navigation">
    												<ul class="pagination mb-0" id="show_page">



    												</ul>
    											</nav>
    										</div>
    										<!-- pagination-wrapper -->
    									</div>



                    </div>
                  </div>
                </div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  <!--footer-->

  @include('admin.component.footer')

  <!-- End Footer-->
</div>
@endsection

@section('page-js')

    <link href="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.css') }}" rel="stylesheet" />
		<link href="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.css') }}" rel="stylesheet" />
    <script src="{{ asset('admins/assets/plugins/sweet-alert/jquery.sweet-modal.min.js') }}"></script>
    <script src="{{ asset('admins/assets/plugins/sweet-alert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('admins/assets/js/sweet-alert.js') }}"></script>
    <script>

    var page_index = 1;
    function loadPage(page_flag){
      page_index = page_flag;
      $.ajax({

          type: 'GET', //THIS NEEDS TO BE GET
          url: '{{route("get_list_depositaddress")}}?page='+page_index,
          dataType: 'json',
          success: function (data) {
            //var obj = JSON.parse(data.data);
            //console.log(obj);
            var obj = JSON.parse(data.data);
            var arraydata = obj.Result;
            var valueColumn='';
            // console.log(data.mess);
            //phan trang\

            if(parseInt(obj.TotalPageCount) > 1){
                html_page ='';
                for(var i = 1 ; i <= parseInt(obj.TotalPageCount); i++){
                    if(page_index == i ){
                        html_page += '<li class="page-item active">';
                    }else{
                        html_page += '<li class="page-item">';
                    }

                    html_page += '<a class="page-link" onclick="loadPage('+i+')">'+i+'</a> </li>';
                }
                $('#show_page').html(html_page);
            }


            var num = 1;
            arraydata.forEach(function(object) {

              //Check URL website.
              if(object.WebUrl=='' || object.WebUrl == undefined){
                webUrl = '...';
              }else{
                webUrl = object.WebUrl;
              }
              valueColumn+= "<tr><td class='align-middle text-center'>"+num+"</td><td class='align-middle text-center'>"+object.ExchangeName+"</td><td class='align-middle text-center'>"+object.Currency+"</td>"+
              '<td class="align-middle text-center"><a class="btn btn-info btn-sm" href="depositaddress/editdepositaddress/'+object.Id+'"><i class="mdi mdi-account-edit"></i></a>' + '<a style="margin-left : 10px" class="btn btn-danger btn-sm delete_conform" id='+object.Id+'><i class="fa fa-trash"></i></a></td>'+
              "<td class='align-middle'>"+object.Address+"</td>"+
              "</tr>"
              num++;
            });
            $(".row-data-load-daily-balances").html(valueColumn);
          },error:function(){
              console.log(data);
          }


      });
    }


      $( document ).ready(function() {
        $("body").on("click",".delete_conform",function(){
            var id = $(this).attr('id');
            swal({
              title: "Xác nhận",
              text: "Bạn đồng ý xoá?",
              type: "warning",
              showCancelButton: true,
              confirmButtonText: 'Có',
              cancelButtonText: 'Không'
            }, function(inputValue) {
                if(inputValue){
                  var url= "/admin/depositaddress/deletedepositaddress/"+id;
                  window.location = url;
                }

            });
        });
      });
    </script>

    <script type="text/javascript">

        $( document ).ready(function() {
          // setInterval(function() {
          // }, 5000);

          $.ajax({

              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_list_depositaddress")}}?page='+page_index,
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='';
                // console.log(data.mess);
                //phan trang\

                if(parseInt(obj.TotalPageCount) > 1){
                    html_page ='';
                    for(var i = 1 ; i <= parseInt(obj.TotalPageCount); i++){
                        if(page_index == i ){
                            html_page += '<li class="page-item active">';
                        }else{
                            html_page += '<li class="page-item">';
                        }

                        html_page += '<a class="page-link" onclick="loadPage('+i+')">'+i+'</a> </li>';
                    }
                    $('#show_page').html(html_page);
                }


                var num = 1;
                arraydata.forEach(function(object) {

                  //Check URL website.
                  if(object.WebUrl=='' || object.WebUrl == undefined){
                    webUrl = '...';
                  }else{
                    webUrl = object.WebUrl;
                  }
                  valueColumn+= "<tr><td class='align-middle text-center'>"+num+"</td><td class='align-middle text-center'>"+object.ExchangeName+"</td><td class='align-middle text-center'>"+object.Currency+"</td>"+
                  '<td class="align-middle text-center"><a class="btn btn-info btn-sm" href="depositaddress/editdepositaddress/'+object.Id+'"><i class="mdi mdi-account-edit"></i></a>' + '<a style="margin-left : 10px" class="btn btn-danger btn-sm delete_conform" id='+object.Id+'><i class="fa fa-trash"></i></a></td>'+
                  "<td class='align-middle'>"+object.Address+"</td>"+
                  "</tr>"
                  num++;
                });
                $(".row-data-load-daily-balances").html(valueColumn);
              },error:function(){
                  console.log(data);
              }


          });


          $.ajax({
              type: 'GET', //THIS NEEDS TO BE GET
              url: '{{route("get_list_exchanges")}}',
              dataType: 'json',
              success: function (data) {
                //var obj = JSON.parse(data.data);
                //console.log(obj);
                var obj = JSON.parse(data.data);
                var arraydata = obj.Result;
                var valueColumn='<option value="">--All--</option>';

                arraydata.forEach(function(object) {
                  //Check URL website.
                  if(object.WebUrl=='' || object.WebUrl == undefined){
                    webUrl = '...';
                  }else{
                    webUrl = object.WebUrl;
                  }
                  if(('{{@$condition['event_id']}}') == object.Id){
                      valueColumn+= '<option selected value="'+object.Id+'">'+object.Name+'</option>';
                  }else{
                    valueColumn+= '<option value="'+object.Id+'">'+object.Name+'</option>';
                  }

                });
                $("#event_id").html(valueColumn);
              },error:function(){
                  console.log(data);
              }
          });

          $('.btn_search').on("click",function(){
              var event_id = $("#event_id").val();
              $("#event_id_val").val(event_id);
              $("#form_search").submit();
          });
        });


    </script>

@endsection
