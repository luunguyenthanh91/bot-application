<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PrimeArbitron BOT @2019</title>


    <link rel="icon" href="{{ asset('front/assets/images/logo.png') }}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">

    <!-- Responsive Stylesheet -->
    <link rel="stylesheet" href="{{ asset('front/css/responsive.css') }}">

    <!-- custom lai css -->
    <link href="{{ asset('front/css/custom.css') }}" rel="stylesheet" type="text/css">




</head>
<body class="darker-blue">



    @yield('content')


    <script src="{{ asset('front/js/jquery.min.js') }} "></script>
    <!-- Popper js -->
    <script src="{{ asset('front/js/popper.min.js') }} "></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('front/js/bootstrap.min.js') }} "></script>
    <!-- All Plugins js -->
    <script src="{{ asset('front/js/plugins.js') }} "></script>
    <!-- Parallax js -->
    <script src="{{ asset('front/js/dzsparallaxer.js') }} "></script>

    <script src="{{ asset('front/js/jquery.syotimer.min.js') }} "></script>

    <!-- script js -->
    <script src="{{ asset('front/js/script.js') }} "></script>


@yield('page-js')
</body>
</html>
