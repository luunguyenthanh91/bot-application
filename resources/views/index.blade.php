@extends('layouts.front')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')





    <!-- ##### Header Area Start ##### -->
    <header class="header-area wow fadeInDown" data-wow-delay="0.2s">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Classy Menu -->
                <nav class="classy-navbar justify-content-between" id="dreamNav">

                    <!-- Logo -->
                    <a class="nav-brand" href="#">
                        <img class="img img-responsive" src="{{ asset('front/assets/images/logo.png') }}" alt="Prime Arbitron" title="Prime Arbitron">
                    </a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- close btn -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
                                <li><a href="#home">TRANG CHỦ</a></li>
                                <li><a href="#about">AI TRADING</a></li>
                                <li><a href="#services">CHÚNG TÔI</a></li>
                                <li><a href="#contact">LIÊN HỆ</a></li>
                                <!-- <li><a href="#team">Team</a></li> -->
                                <!-- <li><a href="#contact">Contact</a></li> -->
                            </ul>

                            <!-- Button -->
                            @if(!Auth::check())
                            <a href="/admin" class="btn login-btn ml-50">Log in</a>
                            @else
                              <a href="/admin" class="btn login-btn ml-50">Dashboard</a>
                            @endif
                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Welcome Area Start ##### -->
    <!-- ##### Welcome Area End ##### -->


    <!-- ##### About Us Area Start ##### -->
    <section class="about-us-area section-padding-100-0 clearfix" id="home">
        <div class="container how-section1">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h4 class="titleheading">
                        <div class="icon">Khám phá một cách mới để tận hưởng Thế giới của bạn!</div>
                    </h4>
                    <h4 class="subheading">TÀI SẢN SỐĐỊNH NGHĨA LẠI TÀI SẢN</h4>
                    <p class="text-muted">Khối lượng giao dịch tiền điện tử hàng ngày đã đạt mốc hơn 50 tỷ USD!
• Vượt mặt tổng vốn hóa hằng năm của top 500 công ty thế giới
• Cao hơn Tổng sản lượng quốc gia(GNP) hằng năm của nhiều quốc gia
• Chiếm lĩnh thị trường chứng khoáng của nhiều quốc gia
• Khiến cho hang ngàn công ty lớn trên thế giới phải tập trung toàn lực vào phát triển công nghệ chỉ trong 1 năm.
• Tạo ra số lượng lớn người có tài sản cao chỉ trong vòng một năm, điều mà không lĩnh vực nào làm được. 
Người ta tin rằng Blockchain và tiền điện tử sẽ cách mạng hóa cách chúng ta trong thế kỷ 21.
</p>

                    <div class="group-btn">
                        <a href="#" class="btn btn-group">Xem thêm</a>
                        <a href="#" class="btn btn-group">Liên hệ</a>
                    </div>
                </div>
                <div class="col-md-6 how-img">
                    <img src="{{ asset('front/assets/images/hero-bg.png') }}" class="img-fluid" alt="" />
                </div>
            </div>
        </div>

        <div class="container how-section1">
            <div class="row align-items-center">
                <div class="col-md-6 col-md-push-6">
                    <h4 class="subheading">CÔNG NGHÊ BLOCKCHAIN TÁI ĐỊNH NGHĨA BẢO MẬT .</h4>
                    <p class="text-muted">Chỉ trong 2 năm, công nghệ Blockchain đã bao phủ 10% các ngành nghề trên thế giới.
Sự xuất hiện của Blockchain đã chính thức chấm dứt một xã hội TậpTrung và chấm dứt các mối quan hệ khó tin tưởng.
</p>

                    <div class="group-btn">
                        <a href="#" class="btn btn-group">Xem thêm</a>
                    </div>
                </div>
                <div class="col-md-6 col-md-pull-6 how-img">
                    <img src="{{ asset('front/assets/images/hero-bg.png') }}" class="img-fluid" alt="" />
                </div>
            </div>
        </div>

        <div class="container how-section1">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h4 class="subheading">TRÍ TUỆ NHÂN TẠO TÁI ĐỊNH NGHĨA THÔNG MINH .</h4>
                    <p class="text-muted">Cách mạngcôngnghệ lầnthứ4 vớisựra đờicủatrí thông minhnhântạo(AI) sẽ gâytácđộng mạnhđếnkinhtế thế giới. Ởgiaiđoạnnày, cáctậpđoàntoàncầu đanggiànhgiựtcôngnghệ AI để đem về nhưng cơhộiquýgiánhằm vượt mặt cácđối thủ trongtươnglai.
Nước Mỹ đang dẫn đầu với 66% lượng nghiên cứu và phát triển công nghệ AI, Trung Quốc đang đứng thứ nhì với 17%. 2 người khổng lồ về công nghệ, Google và Baidu, đã đầu tư hơn 50 tỷ USD vào nghiên cứu và phát triển công nghệ này.
</p>
                    <div class="group-btn">
                        <a href="#" class="btn btn-group">Xem thêm</a>
                    </div>
                </div>
                <div class="col-md-6 how-img">
                    <img src="{{ asset('front/assets/images/robot-3.png') }}" class="img-fluid" alt="" />
                </div>
            </div>
        </div>

        <div class="container how-section1">
            <div class="row align-items-center">
                <div class="col-md-6 col-md-push-6">
                    <h4 class="subheading">CÔNG NGHỆ AI & BLOCKCHAIN CÓ KẾT HỢP ĐƯỢC KHÔNG?</h4>
                    <p class="text-muted">Trí tuệ nhân tạo (AI) là một khái niệm được sử dụng rộng rãi để thực hiện các nhiệm vụ cần trí thông minh và tốc độ. Mặt khác, dữ liệu blockchain được mã hóa và phân tán trên một số máy tính khác nhau; do đó, nó tạo điều kiện cho sự phát triển của cơ sở dữ liệu chống giả mạo và rất mạnh mẽ.</p>
                    <p class="text-muted">Sự kết hợp giữa AI và Blockchain có thể nghe qua hơi lạ và khó khăn nhưng hoàn toàn KHẢ THI!</p>
                    <div class="group-btn">
                        <a href="#" class="btn btn-group">Xem thêm</a>
                    </div>
                </div>
                <div class="col-md-6 col-md-pull-6 how-img">
                    <img src="{{ asset('front/assets/images/hero-bg.png') }}" class="img-fluid" alt="" />
                </div>
            </div>
        </div>

    </section>
    <!-- ##### About Us Area End ##### -->

    <!-- ##### About Us Area Start ##### -->

    <!-- ##### About Us Area End ##### -->

    <!-- section-before section-padding-100-->
    <section id="services" class="demo-video">
        <div class="container how-section1">
            <div class="row col-md-12">
                <div class="col-md-6">
                    <h4 class="subheading">LỢI ÍCH</h4>
                    <p class="text-muted">Sự kết hợp này có thể giúp giải thích các lựa chọn:
 Blockchain hỗ trợ bạn có thể theo dõi, hiểu và biết các lựa chọn bởi AI. Quyết định của AI đôi khi rất khó để hiểu được. Lý do là vì chúng có khả năng xử lý một lượng lớn biến số mà không cần phụ thuộc vào những thứ khác. Để ví dụ, thuật toán của AI có thể tối ưu hóa các quyết định để phân tích các lệnh giao dịch trên sàn là thật hay giả, và có nên phân tích chuyên sâu hay chặn quyền giao dịch.
Đôi khi, dưới vài trường hợp cần thiết, người ta vẫn cần xem xét tính đúng đắn của các lựa chọn bởi AI. Dựa trên lượng lớn dữ liệu có sẵn, điều này có thể tạo nên khó khăn vì sự phức tạp. Nhưng nếu mọi dữ liệu được lưu trữ trên công nghệ blockchain, các nhà nghiên cứu có thể dễ dàng kiểm toán và tự tin rằng mọi quyết định đều không bị tác động từ bên ngoài.
 Việc kiểm soát quá trình đưa ra quyết định trên blockchain giúp công nghệ AI trở nên rõ rang hơn và giúp mọi người hiểu được robot đã dựa trên điều gì để đưa ra các quyết định, giúp gia tăng sự tin tưởng.
</p>
                </div>
                <div class="col-md-6">
                    <img src="{{ asset('front/assets/images/hero-bg.png') }}" class="img-fluid margin-top-30" alt="" />
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

    <!-- ##### Our Services Area Start ##### section-padding-100-0-->
    <section class="our_services_area clearfix" id="about">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-4 mt-5">
                    <img class="img-fluid mb-4" src="{{ asset('front/assets/images/1.svg') }}" alt="">
                    <div class="text-server">
                        <h3 class="head-text-server">PRIMEARBITRON™ TANGO XBOT </h3>
                        <p class="text-muted-server">
                          1. Lấy tin thị trường từ nguồn nổi tiếng nhất <br>
                          2. Hỗ trợ hoạt động 2 sàn cùng một lúc<br>
                          3. Khả năng thay đổi kết nối sàn mới<br>
                          4. 100% thực hiện lệnh giao dịch<br>
                          5. Tự động thực hiện lệnh chống thua lỗ<br>
                          6. Tự động thayđổi vị thế Mua và Bán<br>
                          7. Tự động tối ưu hóa rủi ro<br>
                          8. Tự động quản lý Plugin và thực thi lệnh<br>
                          9. Cập nhật miễn phí<br>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 mt-5">
                    <img class="img-fluid mb-4" src="{{ asset('front/assets/images/2.svg') }}" alt="">
                    <div class="text-server">
                        <h3 class="head-text-server">PRIMEARBITRON™ TRIPLEFLEX BOT</h3>
                        <p class="text-muted-server">
                          1. Chiến thuật Tam Giác Vàng</br>
                          2. 100% giao dịch tự động</br>
                          3. BOT giao dịch Arbitrage</br>
                          4. Hỗ trợ hoạt động 1 lúc 1 sàn cùng một lúc</br>
                          5. Tự động sử dụng các chỉ số</br>
                          6. Tự động quản lý thời gian giao dịch</br>
                          7. Tự động kiểm tra các Plug-in lỗi</br>
                          8. Tự động quản lý các plug-in thực thi</br>
                          9. Tự động quản lý phí Spread</br>
                          10. Tự động dừng lô</br>
                          11. Cập nhật miễn phí</br>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 mt-5">
                    <img class="img-fluid mb-4" src="{{ asset('front/assets/images/3.svg') }}" alt="">
                    <div class="text-server">
                        <h3 class="head-text-server">PRIMEARBITRON™ COIN EXCHANGE</h3>
                        <p class="text-muted-server">
                          1. Lấy ý tưởng từ các sàn p2p nổi tiếng nhất</br>
                          2. Hổ trợ nhiều loại tiền điện tử</br>
                          3. Integrated referral program</br>
                          4. IOS and Android Apps</br>
                          5. E-Wallets</br>
                          6. Chập nhận thanh toán qua nhiều ngân hàng địa phương</br>
                          7. Nhận Visa and Mastercard</br>
                          8. SEO thân thiện</br>
                          9. Cập nhật miễn phí</br>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 mt-5">
                    <img class="img-fluid mb-4" src="{{ asset('front/assets/images/4.svg') }}" alt="" height="58" width="58">
                    <div class="text-server">
                        <h3 class="head-text-server">PRIMEARBITRON™ TRIPLEFEX XBOT</h3>
                        <p class="text-muted-server">
                          1. Chiến thuật Tam Giác Vàng</br>
                          2. 100% giao dịch tự động</br>
                          3. BOT giao dịch Arbitrage</br>
                          4. Hỗ trợ hoạt động 1 lúc 3 sàn cùng một lúc</br>
                          5. Tự động sử dụng các chỉ số</br>
                          6. Tự động quản lý thời gian giao dịch</br>
                          7. Tự động kiểm tra các Plug-in lỗi</br>
                          8. Tự động quản lý các plug-in thực thi</br>
                          9. Tự động quản lý phí Spread</br>
                          10. Tự động dừng lô</br>
                          11. Cập nhật miễn phí</br>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 mt-5">
                    <img class="img-fluid mb-4" src="{{ asset('front/assets/images/5.svg') }}" alt="" height="58" width="58">
                    <div class="text-server">
                        <h3 class="head-text-server">PRIMEARBITRON™ QUADRLEFLEX BOT</h3>
                        <p class="text-muted-server">
                          1. Chiến thuật Tam Giác Vàng<br>
                          2. 100% giao dịch tự động<br>
                          3. BOT giao dịch Arbitrage<br>
                          4. Hỗ trợ hoạt động 1 lúc 1 sàn cùng một lúc<br>
                          5. Tự động sử dụng các chỉ số<br>
                          6. Tự động quản lý thời gian giao dịch<br>
                          7. Tự động kiểm tra các Plug-in lỗi<br>
                          8. Tự động quản lý các plug-in thực thi<br>
                          9. Tự động quản lý phí Spread<br>
                          10. Tự động dừng lô<br>
                          11. Cập nhật miễn phí<br>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 mt-5">
                    <img class="img-fluid mb-4" src="{{ asset('front/assets/images/6.svg') }}" alt="" height="58" width="58">
                    <div class="text-server">
                        <h3 class="head-text-server">PRIMEARBITRON™ QUADRLEFLEX XBOT</h3>
                        <p class="text-muted-server">
                          1. Chiến thuật Tam Giác Vàng<br>
                          2. 100% giao dịch tựđộng<br>
                          3. BOT giao dịch Arbitrage<br>
                          4. Hỗ trợ hoạt động 1 lúc 4 sàn cùng một lúc<br>
                          5. Tự động sử dụng các chỉ số<br>
                          6. Tự động quản lý thời gian giao dịch<br>
                          7. Tự động kiểm tra các Plug-in lỗi<br>
                          8. Tự động quản lý các plug-in thực thi<br>
                          9. Tự động quản lý phí Spread<br>
                          10. Tự động dừng lô<br>
                          11. Cập nhật miễn phí<br>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Our Services Area End ##### -->


    <!-- ##### Our features Area start ##### section-padding-0-100-->
    <section class="features ">

        <div class="container how-section1">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 text-center">
                    <img class="img img-responsive mb-4" src="{{ asset('front/assets/images/surface1.png') }}" alt="" style="margin: 0 auto;">
                    <div class="text-app ">
                        <h5 class="head-text-app ">Ứng Dụng Di Động Của Chúng Tôi</h5>
                        <p class="text-muted-app ">Prime Arbitron TẬP TRUNG ĐẦU TƯ VÀO CÔNG NGHÊ SỬ DỤNG ROBOT AI ĐỂ ÁP DỤNG CÁC PHƯƠNG PHÁP GIAO DỊCH ARBITRAGE</p>
                    </div>
                </div>

                <div class="col-md-12 ">
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-md-push-8 service-img-wrapper ">
                            <div class=" image-box ">
                                <img src="{{ asset('front/img/phone.png') }} " class="center-block img-responsive phone-img " alt=" ">
                                <img src="{{ asset('front/img/core-img/rings-bg.png') }} " class="center-block img-responsive rings " alt=" ">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8 col-md-pull-4 ">
                            <ul class="timeline ">
                                <li>
                                    <div class="timeline-badge ">
                                        <img class="icon-timeline " src="{{ asset('front/assets/images/phone.svg') }} " alt=" " height="48 " width="48 ">
                                    </div>
                                    <div class="timeline-panel ">
                                        <div class="timeline-heading ">
                                            <h4 class="timeline-title ">Ứng Dụng Trực Tuyến Và Di Động Mạnh Mẽ</h4>
                                        </div>
                                        <div class="timeline-body ">
                                            <p>1 . Arbitrage 6 sàn<br>
                                              2 . Theo dõi thị trường<br>
                                              3 . Kiểm soát plugIn<br>
                                              4 . Kiểm soát chênh lệch giá<br>
                                              5 . Tối ưu rủi ro tự động<br>
                                              6 . Phân tích toán học chuyên sâu<br>
                                              7 . Phương pháp Martingale truyền thống<br>
                                              8 . Lưu trữ lịch sử giao dịch<br>
                                              9 . Phân rõ lợi nhuận và vốn giao dịch<br>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="timeline-badge ">
                                        <img class="icon-timeline " src="{{ asset('front/assets/images/surface1-2.svg') }} " alt=" " height="48 " width="48 ">
                                    </div>
                                    <div class="timeline-panel ">
                                        <div class="timeline-heading ">
                                            <h4 class="timeline-title ">Mang Lại Tính Minh Bạch Và Tốc Độ Cao Hơn</h4>
                                        </div>
                                        <div class="timeline-body ">
                                            <p>300 TRIÊU LOẠI PHÂN TÍCH KĨ THUẬT CÙNG VỚI KHẢ NĂNG XỬ LÝ DỮ LIÊU MỖI GIÂY VÀ QUẢN LÝ TRỰC TIẾP HƠN 1000 LOẠI TÀI SẢN SỐ TRÊN THẾ GIỚI
                                            </p>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="timeline-badge ">
                                        <img class="icon-timeline " src="{{ asset('front/assets/images/surface1-1.svg') }}" alt=" " height="48 " width="48 ">
                                    </div>
                                    <div class="timeline-panel ">
                                        <div class="timeline-heading ">
                                            <h4 class="timeline-title ">Đặc Biệt Cho Nhiều Khả Năng Sử Dụng</h4>
                                        </div>
                                        <div class="timeline-body ">
                                            <p>AI Prime Arbitron  nhận thông tin từ hơi 300 triệu nguồn dữ liệu khác nhau, nhiều hơn 1 nhóm các chuyên gia nghiên cứu có thể theo dõi.
                                            </p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Our features Area End ##### -->

    <!-- ##### Subscribe Area start ##### -->
    <section class="story-section ">
        <div class="container ">
            <div class="row ">
                <div class="col-md-12 ">
                    <div class="contact-bg ">
                        <div class="wrapper-contact ">
                            <div class="row ">
                                <div class="col-md-8 col-md-offset-2 ">
                                    <div class="titleheading-contact ">Khám phá một cách mới để tận hưởng Thế giới của bạn!</div>
                                    <p class="text-justify text-cotent-contact ">Biết người biết ta, trăm trận trăm thắng.  Mỗi trận chiến đều định trước kết quả từ trước khi bắt đầu. Câu danh ngôn này đã thường xuyên được nhắc đến, tiêu biểu là trong bộ phim Wall Street bởi Gordon Gekko. Trong bộ phim đó, Gekko đã thu lợi rất nhiều khi áp dụng arbitrage.)  Đáng tiếc, chiến lược giao dịch không rủi ro này không phải ai cũng áp dụng được. Tuy nhiên, từ ý tưởng này nhiều hình thức giao dịch đã được ra đời với tỉ lệ thắng cao hơn. Ở đây chúng ta sẽ bàn bạc về định nghĩa trên lệch, cách mà các nhà tạo lập thị trường tận dụng arbitrage, và cách các nhà đầu tư có thể tận dụng PRIME ARBITRON.</p>
                                    <form action=" " class="frm-contact ">
                                        <div class="form-group ">
                                            <div class="icon-addon addon-md ">
                                                <input type="text " placeholder="Email " class="form-control " id="email ">
                                                <label for="email " class="fa fa-paper-plane " rel="tooltip " title="email "></label>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="wrap-icon-contact ">
                                        <span class="text-icon-contact " style=" ">Trò chuyện trực tiếp qua</span>
                                        <span class="icon-contact ">
                                            <img src="{{ asset('front/assets/images/surface1-5.svg') }} " class="img img-responsive ">
                                        </span>
                                        <span class="icon-contact ">
                                            <img src="{{ asset('front/assets/images/surface1-6.svg') }} " class="img img-responsive ">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="contact-icon-left ">
                            <img src="{{ asset('front/assets/images/img-contact.png') }} " />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Subscribe Area End ##### -->

    <!-- ##### FAQ & Timeline Area Start ##### section-padding-100-->
    <div class="faq-timeline-area " id="faq ">
        <div class="container how-section1 ">
            <div class="row ">
                <div class="col-xs-12 col-ms-12 col-md-6 col-md-offset-3 ">
                    <div class="text-app headding-top-new ">
                        <h5 class="head-text-app ">Các Câu Hỏi Thường Gặp</h5>
                        <p class="text-muted-app ">Arbitrage hiểu đơn giản là mua một sản phẩm tài chính từ một sàn giao dịch và lập tức bán nó đi ở một sàn khác với mức giá cao hơn, nhằm thu lợi lập tức từ việc chênh lệch giá tạm thời. Điều này được xem như là không có rủi ro và đảm bảo lợi nhuận cho nhà đầu tư.</p>
                    </div>
                </div>

                <div class="col-12 col-lg-6 col-md-12 ">

                    <div class="dream-faq-area ">
                        <dl>
                            <!-- Single FAQ Area -->
                            <dt class="wave wow fadeInUp " data-wow-delay="0.2s ">CÔNG NGHỆ AI & BLOCKCHAIN CÓ KẾT HỢP ĐƯỢC KHÔNG?</dt>
                            <dd class="wow fadeInUp " data-wow-delay="0.3s ">
                                <p>Trí tuệ nhân tạo (AI) là một khái niệm được sử dụng rộng rãi để thực hiện các nhiệm vụ cần trí thông minh và tốc độ. Mặt khác, dữ liệu blockchain được mã hóa và phân tán trên một số máy tính khác nhau; do đó, nó tạo điều kiện cho sự phát triển của cơ sở dữ liệu chống giả mạo và rất mạnh mẽ.</p>
                            </dd>
                            <!-- Single FAQ Area -->
                            <dt class="wave wow fadeInUp " data-wow-delay="0.3s ">GIAO DỊCH TẦN SUẤT CAO (HFT) LÀ GÌ? </dt>
                            <dd>
                                <p>HFT là một nền tảng giao dịch trên các hệ thống siêu máy tính được trang bị các thuật toán hết sức phức tạp để phân tích nhiều thị trường cùng một lúc và thực hiện giao dịch dựa trên tình hình. Siêu máy tính có thể thực hiện 1 lượng lớn lệnh giao dịch với tốc độ rất nhanh. Công nghệ AI và HFT sẽ được kết hợp để tiến hành giao dịch Arbitrage. </p>
                            </dd>
                            <!-- Single FAQ Area -->
                            <dt class="wave wow fadeInUp " data-wow-delay="0.4s ">Bảo đảm cho quỹ đầu tư của tôi , Điều đó hoạt động như thế nào?</dt>
                            <dd>
                                <p>Nhà đầu tư hoàn toàn có quyền rút vốn và lợi nhuận của họ khỏi hệ thống sau khi xác nhận thông báo cho tập đoàn trước 7 ngày làm việc.</p>
                            </dd>
                            <!-- Single FAQ Area -->
                            <dt class="wave wow fadeInUp " data-wow-delay="0.5s ">ARBITRAGE LÀ GÌ?</dt>
                            <dd>
                                <p>Arbitrage hiểu đơn giản là mua một sản phẩm tài chính từ một sàn giao dịch và lập tức bán nó đi ở một sàn khác với mức giá cao hơn, nhằm thu lợi lập tức từ việc chênh lệch giá tạm thời. Điều này được xem như là không có rủi ro và đảm bảo lợi nhuận cho nhà đầu tư.</p>
                            </dd>
                        </dl>
                    </div>
                </div>

                <div class="col-12 col-lg-6 ">
                    <img src="{{ asset('front/img/svg/faq.svg') }} " class="img-responsive center-block " alt=" ">
                </div>
            </div>
        </div>
    </div>
    <!-- ##### FAQ & Timeline Area End ##### -->


    <!-- ##### Mission Area Start ##### -->
    <section class="engaged-section social ">
        <div class="container text-center ">
            <div class="row ">
                <div class="col-md-4 mt-5 col-padding0 ">
                    <img class="rounded-circle mb-4 " src="{{ asset('front/assets/images/surface1-8.svg') }} " alt=" " height="58 " >
                    <div class="text-server ">
                        <h3 class="head-text-server ">PRIME ARBITRON™ CRYPTO BOT</h3>
                        <p class="text-muted-server ">
                          1. Hỗ trợ các sàn lớn ( chọn lựa sàn lớn, uy tín)<br>
                          2. Hỗ trợ hơn 100-1000 cặp tiền khác nhau<br>
                          3. Giao diện đơn giản, không cần cài đặt<br>
                          4. Đã hỗ trợ cài đặt sẵn 6 sàn<br>
                          5. Hỗ trợ hoạt động 1 lúc 6 sàn trở lên<br>
                          6. Cho phép theo dõi tín hiệu chêch lệch giá trên các sàn giao dịch<br>
                          7. Tự động giao dịch Arbitrage<br>
                          8. Tự động thực hiện khớp lệnh<br>
                          9. Cho phép chạy lệnh thử để xem hiệu quả <br>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 mt-5 col-padding0 ">
                    <img class="rounded-circle mb-4 " src="{{ asset('front/assets/images/surface1-7.svg') }} " alt=" " height="58 " >
                    <div class="text-server ">
                        <h3 class="head-text-server ">Prime Arbitron</h3>
                        <p class="text-muted-server ">
                          1.TẬP TRUNG ĐẦU TƯ VÀO CÔNG NGHÊ SỬ DỤNG ROBOT AI ĐỂ ÁP DỤNG CÁC PHƯƠNG PHÁP GIAO DỊCH ARBITRAGE<br>
                          2. PRIME ARBITRON phân tích các phương pháp giao dịch của các tổ chức Fintech và Tài chính lớn khắp châu Á và châu Âu <br>
                          3.PRIME ARBITRON  là phần mềm được nghiên cứu bí mật trong 2 năm qua để đáp ứng các cơ hội thu lợi từ thị trường thông qua Arbitrage
                        </p>
                    </div>
                </div>
                <div class="col-md-4 mt-5 col-padding0 ">
                    <img class="rounded-circle mb-4 " src="{{ asset('front/assets/images/surface1-9.svg') }} " alt=" " height="58 " >
                    <div class="text-server ">
                        <h3 class="head-text-server ">120 CÔNG TY AI STARTUPS</h3>
                        <p class="text-muted-server ">Năm 2018, DOANH THU CỦA ỨNG DỤNG CÔNG NGHÊ AI TRONG CÁC NGÀNH CÔNG NGHIỆP ĐẠT TỚI 57.7 TỶ USD!</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- ##### Mission Area End ##### -->



    <!-- ##### Blog Area Start ##### -->
    <section class="our_blog_area clearfix " id="blog ">
        <div class="container ">
            <div class="row ">
                <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 ">
                    <div class="text-center headding-top-new ">
                        <h2 class="wow fadeInUp " data-wow-delay="0.3s ">Tin Mới Nhất</h2>
                        <p class="wow fadeInUp " data-wow-delay="0.4s ">
                          Cung cấp và tối ưu công nghệ tốt nhất trên toàn cầu để tìm ra cơ hội sinh lời tốt nhất cho khách hàng của chúng tôi.<br>
                          Chúng tôi luôn đưa ra thành quả ổn định trong các lựa chọn đầu tư của mình bất kể trong mảng kinh tế tài chính nào.

                        </p>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center ">
                <!-- Single Blog Post -->
                <div class="col-xs-12 col-md-6 col-lg-4 ">
                    <div class="single-blog-area wow fadeInUp " data-wow-delay="0.2s ">
                        <!-- Post Thumbnail -->
                        <div class="blog_thumbnail ">
                            <img src="{{ asset('front/assets/images/Group84.png') }} " alt=" ">
                        </div>
                        <!-- Post Content -->
                        <div class="blog-content ">
                            <!-- Dream Dots -->

                            <div class="post-meta mt-20 ">
                                <p>By <a href="# " class="post-author ">ADMIN</a> <a href="# ">Apr 10, 2018</a> <a href="# " class="post-comments ">7 comments</a></p>
                            </div>
                            <a href="index-single-blog.html " class="post-title ">
                                <h4>Chỉ số quan trọng cho thấy đã đến lúc tích lũy Bitcoin, nhưng đáy vẫn còn rất xa.</h4>
                            </a>
                            <p>Mặc dù Bitcoin đã biến động khá nhiều trong ngày hôm qua nhưng giá đã ổn định quanh mức 7,200 đô la. Theo đó, một nhà phân tích kỹ thuật nổi tiếng cho rằng bây giờ là thời điểm tốt để xếp chồng thêm sats, trong khi một người khác ước tính giá sẽ điều chỉnh thêm.</p>
                        </div>
                    </div>
                </div>

                <!-- Single Blog Post -->
                <div class="col-12 col-md-6 col-lg-4 ">
                    <div class="single-blog-area wow fadeInUp " data-wow-delay="0.3s ">
                        <!-- Post Thumbnail -->
                        <div class="blog_thumbnail ">
                            <img src="{{ asset('front/assets/images/Group85.png') }} " alt=" ">
                        </div>
                        <!-- Post Content -->
                        <div class="blog-content ">
                            <!-- Dream Dots -->

                            <div class="post-meta mt-20 ">
                                <p>By <a href="# " class="post-author ">ADMIN</a> <a href="# ">Apr 10, 2018</a> <a href="# " class="post-comments ">7 comments</a></p>
                            </div>
                            <a href="l " class="post-title ">
                                <h4>Đức sẽ ban hành quy định về tiền điện tử vào ngày 01/01/2020.</h4>
                            </a>
                            <p>“Đại diện kỹ thuật số của một giá trị không được phát hành hoặc bảo đảm bởi Ngân hàng Trung ương hoặc cơ quan công quyền và không có tư cách pháp lý như một loại tiền tệ, nhưng được chấp nhận bởi các thể nhân hoặc pháp nhân, dựa trên thỏa thuận hoặc thực tiễn, như là phương tiện thanh toán, trao đổi hoặc được sử dụng cho mục đích đầu tư và được di chuyển, lưu trữ và giao dịch điện tử”.</p>
                        </div>
                    </div>
                </div>

                <!-- Single Blog Post -->
                <div class="col-12 col-md-6 col-lg-4 ">
                    <div class="single-blog-area wow fadeInUp " data-wow-delay="0.4s ">
                        <!-- Post Thumbnail -->
                        <div class="blog_thumbnail ">
                            <img src="{{ asset('front/assets/images/Group86.png') }} " alt=" ">
                        </div>
                        <!-- Post Content -->
                        <div class="blog-content ">
                            <!-- Dream Dots -->

                            <div class="post-meta mt-20 ">
                                <p>By <a href="# " class="post-author ">ADMIN</a> <a href="# ">Apr 10, 2018</a> <a href="# " class="post-comments ">7 comments</a></p>
                            </div>
                            <a href="index-single-blog.html " class="post-title ">
                                <h4>Fractal Ethereum báo hiệu chu kỳ thị trường mới sẽ sớm bắt đầu.</h4>
                            </a>
                            <p>Ethereum đã không xử lý tình huống tốt khi Bitcoin giảm giá gần đây. Nó đã giảm với tốc độ nhanh hơn nhiều so với các coin dẫn đầu thị trường tiền điện tử, bằng chứng là cặp ETH/USD thua lỗ 12%.

Có rất nhiều lý do gây sụt giảm nhanh hơn dự kiến ​​như đã nói. Đầu tiên, một ví được liên kết với PlusToken vừa chuyển hơn 100 triệu đô la ETH.

Ngoài ra, tin tức cho biết một nhà đầu tư ICO Ethereum đã chuyển gần 300.000 ETH ra khỏi tài khoản được lập từ năm 2015....</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Blog Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area bg-img " style="background-image: url(img/core-img/pattern.png); ">

        <!-- ##### Contact Area Start ##### -->
        <div class="contact_us_area " id="contact">
            <div class="container ">
                <div class="row ">
                    <div class="col-12 ">
                        <div class="section-heading text-center ">

                            <h2 class="wow fadeInUp " data-wow-delay="0.3s ">Liên hệ với chúng tôi</h2>
                            <p class="wow fadeInUp " data-wow-delay="0.4s ">Được thành lập vào năm 2018 tại Singapore, PRIME ARBITRON là một trong những công ty FinTech hàng đầu, với hơn 100 triệu tài sản và quỹ được quản lý trên toàn thế giới. PRIME ARBITRON đang phát triển nhanh chóng để trở thành một trong những công ty hàng đầu về Công nghệ Tài chính.</p>
                        </div>
                    </div>
                </div>

                <!-- Contact Form -->
                <div class="row justify-content-center ">
                    <div class="col-12 col-md-10 col-lg-8 ">
                        <div class="contact_form ">
                            <form action="# " method="post " id="main_contact_form " novalidate>
                                <div class="row ">
                                    <div class="col-12 ">
                                        <div id="success_fail_info "></div>
                                    </div>

                                    <div class="col-12 col-md-6 ">
                                        <div class="group wow fadeInUp " data-wow-delay="0.2s ">
                                            <input type="text " name="name " id="name " required>
                                            <span class="highlight "></span>
                                            <span class="bar "></span>
                                            <label>Họ Tên</label>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 ">
                                        <div class="group wow fadeInUp " data-wow-delay="0.3s ">
                                            <input type="text " name="email " id="email " required>
                                            <span class="highlight "></span>
                                            <span class="bar "></span>
                                            <label>Mail</label>
                                        </div>
                                    </div>
                                    <div class="col-12 ">
                                        <div class="group wow fadeInUp " data-wow-delay="0.4s ">
                                            <input type="text " name="subject " id="subject " required>
                                            <span class="highlight "></span>
                                            <span class="bar "></span>
                                            <label>Vấn đề</label>
                                        </div>
                                    </div>
                                    <div class="col-12 ">
                                        <div class="group wow fadeInUp " data-wow-delay="0.5s ">
                                            <textarea name="message " id="message " required></textarea>
                                            <span class="highlight "></span>
                                            <span class="bar "></span>
                                            <label>Nội dung</label>
                                        </div>
                                    </div>
                                    <div class="col-12 text-center wow fadeInUp " data-wow-delay="0.6s ">
                                        <button type="submit " class="btn dream-btn ">GỞI THƯ</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ##### Contact Area End ##### -->

        <div class="footer-content-area ">
            <div class="container ">
                <div class="row align-items-end ">
                    <div class="col-12 col-md-5 ">
                        <div class="footer-copywrite-info ">
                            <!-- Copywrite -->
                            <div class="copywrite_text wow fadeInUp " data-wow-delay="0.2s ">
                                <div class="footer-logo ">
                                    <a href="# "><img src="{{ asset('front/assets/images/logo-lag.png') }} " class="img img-responsive " alt="Prime Arbitron " title="Prime Arbitron "></a>
                                </div>
                                <p>Biết người biết ta, trăm trận trăm thắng.  Mỗi trận chiến đều định trước kết quả từ trước khi bắt đầu. Câu danh ngôn này đã thường xuyên được nhắc đến, tiêu biểu là trong bộ phim Wall Street bởi Gordon Gekko. Trong bộ phim đó, Gekko đã thu lợi rất nhiều khi áp dụng arbitrage.)  Đáng tiếc, chiến lược giao dịch không rủi ro này không phải ai cũng áp dụng được. Tuy nhiên, từ ý tưởng này nhiều hình thức giao dịch đã được ra đời với tỉ lệ thắng cao hơn. Ở đây chúng ta sẽ bàn bạc về định nghĩa trên lệch, cách mà các nhà tạo lập thị trường tận dụng arbitrage, và cách các nhà đầu tư có thể tận dụng PRIME ARBITRON.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-7 ">
                        <!-- Content Info -->
                        <div class="contact_info_area d-sm-flex justify-content-between ">
                            <div class="contact_info text-center wow fadeInUp " data-wow-delay="0.2s ">
                                <h5>NAVIGATE</h5>
                                <a href=" ">
                                    <p>Advertisers</p>
                                </a>
                                <a href=" ">
                                    <p>Developers</p>
                                </a>
                                <a href=" ">
                                    <p>Resources</p>
                                </a>
                                <a href=" ">
                                    <p>Company</p>
                                </a>
                                <a href=" ">
                                    <p>Connect</p>
                                </a>
                            </div>
                            <!-- Content Info -->
                            <div class="contact_info text-center wow fadeInUp " data-wow-delay="0.3s ">
                                <h5>PRIVACY & TOS</h5>
                                <a href=" ">
                                    <p>Advertiser Agreement</p>
                                </a>
                                <a href=" ">
                                    <p>Acceptable Use Policy</p>
                                </a>
                                <a href=" ">
                                    <p>Privacy Policy</p>
                                </a>
                                <a href=" ">
                                    <p>Technology Privacy</p>
                                </a>
                                <a href=" ">
                                    <p>Developer Agreement</p>
                                </a>
                            </div>
                            <!-- Content Info -->
                            <div class="contact_info text-center wow fadeInUp " data-wow-delay="0.4s ">
                                <h5>CONTACT US</h5>
                                <p>Mailing Address:xx00 E. Union Ave</p>
                                <p>Suite 1100. Denver, CO 80237</p>
                                <p>+999 90932 627</p>
                                <p>support@yourdomain.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->

@endsection
